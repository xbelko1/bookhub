using System.Text;
using BusinessLayer.Extensions;
using DataAccessLayer.Data;
using DataAccessLayer.Entities;
using DataAccessLayer.Extensions;
using Infrastructure.Middleware.Logger;
using Infrastructure.Middleware.ResponseTransformer;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using WebApi.Extensions;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddDbContextFactory<BookHubDBContext>(options =>
{
    options
        .UseNpgsql(builder.Configuration.GetConnectionString("DatabaseConnectionString"))
        .UseLazyLoadingProxies()
        ;
});

builder.Services.AddDefaultIdentity<User>(options =>
    {
        options.SignIn.RequireConfirmedAccount = false;
        options.Password.RequireNonAlphanumeric = false;
        options.Password.RequiredLength = 4;
    })
    .AddEntityFrameworkStores<BookHubDBContext>();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"]!)),
            ValidateIssuer = false,
            ValidateAudience = false
        };
    });

builder.Services.RegisterServices();
builder.Services.RegisterRepositories();
builder.Services.RegisterSwagger();

builder.Logging.AddConsole();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseMiddleware<LoggerMiddleware>();
app.UseMiddleware<ResponseTransformer>();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
