using BusinessLayer.DTOs.Review;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[ApiController]
[Authorize]
[Route("reviews")]
public class ReviewController : BaseController
{
    private readonly ReviewService _reviewService;

    public ReviewController(UserManager<User> userManager, ReviewService reviewService) : base(userManager)
    {
        _reviewService = reviewService;
    }
    
    [HttpGet]
    public async Task<IActionResult> GetAllReviews()
    {
        var serviceResult = await _reviewService.GetAllReviews();
        return BuildResponse(serviceResult);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetReview(int id)
    {
        var serviceResult = await _reviewService.GetReviewById(id);
        return BuildResponse(serviceResult);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteReview(int id)
    {
        var serviceResult = await _reviewService.DeleteReview(id);
        return BuildResponse(serviceResult);
    }
    
    [HttpPost]
    public async Task<IActionResult> AddReview(ReviewCreateDto reviewCreateDto)
    {
        var userId = await GetUserId();
        var serviceResult = await _reviewService.AddReview(userId, reviewCreateDto);
        return BuildResponse(serviceResult);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateReview(int id, ReviewCreateDto reviewCreateDto)
    {
        var serviceResult = await _reviewService.UpdateReview(id, reviewCreateDto);
        return BuildResponse(serviceResult);
    }
}
