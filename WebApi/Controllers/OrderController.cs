using BusinessLayer.DTOs.Order;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[ApiController]
[Authorize]
[Route("orders")]
public class OrderController: BaseController
{
    private readonly OrderService _orderService;

    public OrderController(UserManager<User> userManager, OrderService orderService) : base(userManager)
    {
        _orderService = orderService;
    }
    
    [HttpPost]
    public async Task<IActionResult> AddOrder(OrderCreateDto orderCreateDto)
    {
        var userId = await GetUserId();
        var serviceResult = await _orderService.AddOrder(orderCreateDto, userId);
        return BuildResponse(serviceResult);
    }
    
    [HttpGet("myOrders")]
    public async Task<IActionResult> GetAllUserOrders()
    {
        var userId = await GetUserId();
        var serviceResult = await _orderService.GetAllUserOrders(userId);
        return BuildResponse(serviceResult);
    }
    
    [HttpGet]
    public async Task<IActionResult> GetAllOrders()
    {
        var serviceResult = await _orderService.GetAllOrders();
        return BuildResponse(serviceResult);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetUserOrder(int id)
    {
        var userId = await GetUserId();
        var serviceResult = await _orderService.GetOrderById(id, userId);
        return BuildResponse(serviceResult);
    }
}