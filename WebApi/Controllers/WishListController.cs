using BusinessLayer.DTOs.WishList;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[ApiController]
[Authorize]
[Route("wishLists")]
public class WishListController : BaseController
{
    private readonly WishListService _wishListService;

    public WishListController(UserManager<User> userManager, WishListService wishListService) : base(userManager)
    {
        _wishListService = wishListService;
    }
    
    [HttpGet]
    public async Task<IActionResult> GetAllWishLists()
    {
        var serviceResult = await _wishListService.GetAllWishLists();
        return BuildResponse(serviceResult);
    }
    
    [HttpPost]
    public async Task<IActionResult> AddWishList(WistListCreateDto wishList)
    {
        var userId = await GetUserId();
        
        var serviceResult = await _wishListService.AddWishList(userId, wishList);
        return BuildResponse(serviceResult);
    }

    [HttpPost("{wishListId}/add-book/{bookId}")]
    public async Task<IActionResult> AddBookToWishList(int wishListId, int bookId)
    {
        var serviceResult = await _wishListService.AddBookToWishList(wishListId, bookId);
        return BuildResponse(serviceResult);
    }
    
    [HttpDelete("{wishListId}/delete-book/{bookId}")]
    public async Task<IActionResult> DeleteBookFromWishList(int wishListId, int bookId)
    {
        var serviceResult = await _wishListService.DeleteBookFromWishList(wishListId, bookId);
        return BuildResponse(serviceResult);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetWishList(int id)
    {
        var serviceResult = await _wishListService.GetWishListById(id);
        return BuildResponse(serviceResult);
    }
    
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteWishList(int id)
    {
        var serviceResult = await _wishListService.DeleteWishList(id);
        return BuildResponse(serviceResult);
    }
    
    
}