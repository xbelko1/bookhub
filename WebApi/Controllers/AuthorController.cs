using BusinessLayer.DTOs.Author;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[ApiController]
[Authorize]
[Route("authors")]
public class AuthorController : BaseController
{
    private readonly AuthorService _authorService;

    public AuthorController(UserManager<User> userManager, AuthorService authorService) : base(userManager)
    {
        _authorService = authorService;
    }

    [HttpGet]
    public async Task<IActionResult> GetAllAuthors()
    {
        var serviceResult = await _authorService.GetAllAuthors();
        return BuildResponse(serviceResult);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetAuthor(int id)
    {
        var serviceResult = await _authorService.GetAuthorById(id);
        return BuildResponse(serviceResult);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteAuthor(int id)
    {
        var serviceResult = await _authorService.DeleteAuthor(id);
        return BuildResponse(serviceResult);
    }

    [HttpPost]
    public async Task<IActionResult> AddAuthor(AuthorCreateDto authorCreateDto)
    {
        var serviceResult = await _authorService.AddAuthor(authorCreateDto);
        return BuildResponse(serviceResult);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateAuthor(int id, AuthorCreateDto authorCreateDto)
    {
        var serviceResult = await _authorService.UpdateAuthor(id, authorCreateDto);
        return BuildResponse(serviceResult);
    }
}
