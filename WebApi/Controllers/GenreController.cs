using BusinessLayer.DTOs.Genre;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[ApiController]
[Route("genres")]
public class GenreController : BaseController
{
    private readonly GenreService _genreService;

    public GenreController(UserManager<User> userManager, GenreService genreService) : base(userManager)
    {
        _genreService = genreService;
    }
    
    [HttpGet]
    public async Task<IActionResult> GetAllGenres()
    {
        var serviceResult = await _genreService.GetAllGenres();
        return BuildResponse(serviceResult);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetGenre(int id)
    {
        var serviceResult = await _genreService.GetGenreById(id);
        return BuildResponse(serviceResult);
    }

    [HttpPost]
    public async Task<IActionResult> AddGenre(GenreCreateDto genreCreateDto)
    {
        var serviceResult = await _genreService.AddGenre(genreCreateDto);
        return BuildResponse(serviceResult);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteGenre(int id)
    {
        var serviceResult = await _genreService.DeleteGenre(id);
        return BuildResponse(serviceResult);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateGenre(int id, GenreCreateDto genreCreateDto)
    {
        var serviceResult = await _genreService.UpdateGenre(id, genreCreateDto);

        return BuildResponse(serviceResult);
    }
}