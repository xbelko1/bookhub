using BusinessLayer.DTOs.Book;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[ApiController]
[Authorize]
[Route("books")]
public class BookController : BaseController
{
    private readonly BookService _bookService;

    public BookController(UserManager<User> userManager, BookService bookService) : base(userManager)
    {
        _bookService = bookService;
    }
    
    [HttpGet]
    public async Task<IActionResult> GetAllBooks()
    {
        var serviceResult = await _bookService.GetAllBooks();
        return BuildResponse(serviceResult);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetBook(int id)
    {
        var serviceResult = await _bookService.GetBookById(id);
        return BuildResponse(serviceResult);
    }

    [HttpPost]
    public async Task<IActionResult> AddBook(BookCreateDto book)
    {
        var serviceResult = await _bookService.AddBook(book);
        return BuildResponse(serviceResult);
    }
    
    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateBook(int id, BookCreateDto updatedBook)
    {
        var serviceResult = await _bookService.UpdateBook(id, updatedBook);
        return BuildResponse(serviceResult);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteBook(int id)
    {
        var serviceResult = await _bookService.DeleteBook(id);
        return BuildResponse(serviceResult);
    }
    
    [HttpGet("filter")]
    public async Task<IActionResult> FilterBooks([FromQuery] BookFilterCriteria filterCriteria)
    {
        var serviceResult = await _bookService.FilterBooks(filterCriteria);
        return BuildResponse(serviceResult);
    }
}
