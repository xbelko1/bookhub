using BusinessLayer.DTOs.User;
using BusinessLayer.Enums;
using BusinessLayer.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[ApiController]
[Route("auth")]
public class AuthController : Controller
{
    private readonly AuthService _authService;

    public AuthController(AuthService authService)
    {
        _authService = authService;
    }
    
    [HttpPost("login")]
    public async Task<IActionResult> Login(LoginUserDto userDto)   
    {
        var result = await _authService.LoginAsync(userDto);

        if (result.ResultCode == ResultCode.Ok)
        {
            return Ok(new { token = result.Data });
        }

        return StatusCode((int)result.ResultCode, result.ErrorMessage);
    }

    [HttpPost("register")]
    public async Task<IActionResult> Register(RegisterUserDto userDto)
    {
        var result = await _authService.RegisterAsync(userDto);
        
        if (result.ResultCode == ResultCode.Ok)
        {
            return Ok(new { token = result.Data });
        }

        return StatusCode((int)result.ResultCode, result.ErrorMessage);
    }
}