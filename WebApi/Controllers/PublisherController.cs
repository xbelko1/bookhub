using BusinessLayer.DTOs.Publisher;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[ApiController]
[Route("publishers")]
public class PublisherController : BaseController
{
    private readonly PublisherService _publisherService;

    public PublisherController(UserManager<User> userManager, PublisherService publisherService) : base(userManager)
    {
        _publisherService = publisherService;
    }
    
    [HttpGet]
    public async Task<IActionResult> GetAllPublishers()
    {
        var serviceResult = await _publisherService.GetAllPublishers();
        return BuildResponse(serviceResult);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetPublisher(int id)
    {
        var serviceResult = await _publisherService.GetPublisherById(id);
        return BuildResponse(serviceResult);
    }

    [HttpPost]
    public async Task<IActionResult> AddPublisher(PublisherCreateDto publisherCreateDto)
    {
        var serviceResult = await _publisherService.AddPublisher(publisherCreateDto);
        return BuildResponse(serviceResult);
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeletePublisher(int id)
    {
        var serviceResult = await _publisherService.DeletePublisher(id);
        return BuildResponse(serviceResult);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdatePublisher(int id, PublisherCreateDto publisherCreateDto)
    {
        var serviceResult = await _publisherService.UpdatePublisher(id, publisherCreateDto);

        return BuildResponse(serviceResult);
    }
}