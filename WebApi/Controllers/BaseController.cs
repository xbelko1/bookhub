using BusinessLayer.Enums;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

public abstract class BaseController : Controller
{
    private readonly UserManager<User> _userManager;

    protected BaseController(UserManager<User> userManager)
    {
        _userManager = userManager;
    }

    protected async Task<int> GetUserId()
    {
        var user = await _userManager.GetUserAsync(User);
        
        return user.Id;
    }
    
    protected IActionResult BuildResponse<T>(ServiceResult<T> serviceResult)
    {
        if (serviceResult.ResultCode == ResultCode.Ok)
        {
            return Ok(serviceResult.Data);
        }

        return StatusCode((int)serviceResult.ResultCode, serviceResult.ErrorMessage);
    }
}