using BusinessLayer.Services;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[ApiController]
[Authorize]
[Route("users")]
public class UserController : BaseController
{
    private readonly UserService _userService;

    public UserController(UserService userService, UserManager<User> userManager) : base(userManager)
    {
        _userService = userService;
    }

    [HttpGet("info")]
    public async Task<IActionResult> GetUserInformation()
    {
        var userId = await GetUserId();
        var user = await _userService.GetCurrentUser(userId);

        return Ok(new
        {
            user.CustomerInfo.FirstName,
            user.CustomerInfo.LastName,
            user.Email,
        });
    }
}