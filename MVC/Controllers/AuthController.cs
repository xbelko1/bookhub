using BusinessLayer.DTOs.User;
using BusinessLayer.Enums;
using BusinessLayer.Services;
using Microsoft.AspNetCore.Mvc;
using MVC.Models.Auth;

namespace MVC.Controllers;

public class AuthController : Controller
{
    private readonly AuthService _authService;

    public AuthController(AuthService authService)
    {
        _authService = authService;
    }

    public IActionResult Register()
    {
        return View();
    }
    
    [HttpPost]
    public async Task<IActionResult> Register(RegisterViewModel model)
    {
        if (ModelState.IsValid)
        {
            var user = new RegisterUserDto { UserName = model.UserName, Password = model.Password };
            var result = await _authService.RegisterAsync(user);
            
            if (result.ResultCode == ResultCode.Ok)
            {
                return RedirectToAction(nameof(Login), nameof(AuthController).Replace("Controller", ""));
            }

            ModelState.AddModelError(string.Empty, result.ErrorMessage);
        }

        return View(model);
    }
    
    public IActionResult Login()
    {
        return View();
    }
    
    [HttpPost]
    public async Task<IActionResult> Login(LoginViewModel model)
    {
        if (ModelState.IsValid)
        {
            var user = new LoginUserDto { UserName = model.UserName, Password = model.Password };
            var result = await _authService.LoginAsync(user);
            if (result.ResultCode == ResultCode.Ok)
            {
                return RedirectToAction(nameof(LoginSuccess),nameof(AuthController).Replace("Controller", ""));
            }

            ModelState.AddModelError(string.Empty, "Invalid login attempt.");
        }

        return View(model);
    }
    
    public async Task<IActionResult> LoginSuccess()
    {
        var user = await _authService.GetUserAsync(User);
        
        ViewBag.Token = _authService.CreateToken(user.UserName, user.Id);
        ViewBag.Username = user.UserName;
        
        return View();
    }
    
    public async Task<IActionResult> Logout()
    {
        await _authService.SignOutAsync();
        return RedirectToAction(nameof(HomeController.Index), nameof(HomeController).Replace("Controller", ""));
    }

}