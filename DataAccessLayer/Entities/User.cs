using Microsoft.AspNetCore.Identity;

namespace DataAccessLayer.Entities;

public class User : IdentityUser<int>
{
    public virtual CustomerInfo CustomerInfo { get; set; }
    
    public virtual IEnumerable<Order> Orders { get; set; } = new List<Order>();
    
    public virtual IEnumerable<WishList>? WishLists { get; set; }
    
    public virtual IEnumerable<Review>? Reviews { get; set; }
}
