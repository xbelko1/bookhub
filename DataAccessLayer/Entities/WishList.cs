using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities;

public class WishList : BaseEntity
{
    public string Name { get; set; }
    
    public int UserId { get; set; }

    [ForeignKey(nameof(UserId))]
    public virtual User? User { get; set; }

    public virtual ICollection<WishListItem> Items { get; set; } = new List<WishListItem>();
}