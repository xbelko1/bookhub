namespace DataAccessLayer.Entities;

public class OrderItem: BaseEntity
{
    public int Amount { get; set; }
    public decimal PricePerPiece { get; set; }
    
    public int BookId { get; set; }
    public virtual Book Book { get; set; }
    
    public int OrderId { get; set; }
    public virtual Order Order { get; set; }
}