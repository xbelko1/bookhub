namespace DataAccessLayer.Entities;

public class WishListItem : BaseEntity
{
    public int WishlistId { get; set; }
    public int BookId { get; set; }

    public virtual WishList WishList { get; set; }
    public virtual Book Book { get; set; }
}