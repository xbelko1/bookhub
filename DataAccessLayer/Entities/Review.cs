using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities;

public class Review : BaseEntity
{
    public string Text { get; set; }
    public double Rating { get; set; }
    
    public int? UserId { get; set; }
    [ForeignKey(nameof(UserId))]
    public virtual User? User { get; set; }
    
    public int BookId { get; set; }
    [ForeignKey(nameof(BookId))]
    public virtual Book Book { get; set; }
}
