using Microsoft.AspNetCore.Identity;

namespace DataAccessLayer.Entities;
public class CustomerInfo: BaseEntity
{
    [PersonalData]
    public int PostCode { get; set; }
    [PersonalData]
    public string Street { get; set; }
    [PersonalData]
    public string City { get; set; }
    [PersonalData]
    public int HouseNumber { get; set; }
    [PersonalData]
    public string FirstName { get; set; }
    [PersonalData]
    public string LastName { get; set; }
    [PersonalData]
    public string Email { get; set; }

    public int UserId { get; set; }
    public virtual User User { get; set; }
    
}