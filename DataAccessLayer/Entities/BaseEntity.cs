using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Entities;

public abstract class BaseEntity
{
    [Key]
    public int Id { get; set; }
}