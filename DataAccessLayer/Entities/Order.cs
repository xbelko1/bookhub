namespace DataAccessLayer.Entities;

public class Order: BaseEntity
{
    public virtual IEnumerable<OrderItem> OrderItems { get; set; } = new List<OrderItem>();
    public DateTime Date { get; set; } = DateTime.Now;

    public int UserId { get; set; }
    public virtual User User { get; set; }
}