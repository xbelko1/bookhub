namespace DataAccessLayer.Entities;

public class Book : BaseEntity
{
    public string Name { get; set; }
    public int PublishYear { get; set; }
    public decimal Price { get; set; }
    public string Description { get; set; }
    public int PageNumber { get; set; }

    public int GenreId { get; set; }
    public virtual Genre Genre { get; set; }
    
    public int PublisherId { get; set; }
    public virtual Publisher Publisher { get; set; }
    public int AuthorId { get; set; }
    public virtual Author Author { get; set; }
    
    public virtual IEnumerable<OrderItem> OrderItems { get; set; } = new List<OrderItem>();
    public virtual IEnumerable<Review>? Reviews { get; set; }
}
