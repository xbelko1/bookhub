using DataAccessLayer.Data;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;

namespace DataAccessLayer.Repositories;

public class PublisherRepository: GenericRepository<Publisher>, IPublisherRepository
{
    public PublisherRepository(BookHubDBContext context): base(context) { }
}