using DataAccessLayer.Data;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;

namespace DataAccessLayer.Repositories;

public class WishListRepository : GenericRepository<WishList>, IWishListRepository
{
    public WishListRepository(BookHubDBContext context): base(context) { }
}