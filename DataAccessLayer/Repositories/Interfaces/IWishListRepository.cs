using DataAccessLayer.Entities;

namespace DataAccessLayer.Repositories.Interfaces;

public interface IWishListRepository: IRepository<WishList> { }