using DataAccessLayer.Entities;

namespace DataAccessLayer.Repositories.Interfaces;

public interface IBookRepository: IRepository<Book> { }