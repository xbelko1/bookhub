using System.Linq.Expressions;
using DataAccessLayer.Entities;

namespace DataAccessLayer.Repositories.Interfaces;

public interface IRepository<TEntity>
    where TEntity : BaseEntity
{
    Task<TEntity> Add(TEntity entity);
    Task<TEntity?> GetById(int id);
    Task<IEnumerable<TEntity>> GetAll();
    Task<TEntity> Delete(TEntity entity);
    Task<TEntity> Update(TEntity entity);
    Task<bool> Exists(int id);
    Task<IEnumerable<TEntity>> Filter(Predicate<TEntity> filter);
}