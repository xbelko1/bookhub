using DataAccessLayer.Entities;

namespace DataAccessLayer.Repositories.Interfaces;

public interface IReviewRepository: IRepository<Review> { }