using DataAccessLayer.Entities;

namespace DataAccessLayer.Repositories.Interfaces;

public interface IPublisherRepository: IRepository<Publisher> { }