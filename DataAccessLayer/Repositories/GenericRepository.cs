using System.Linq.Expressions;
using DataAccessLayer.Data;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Repositories;

public abstract class GenericRepository<TEntity> : IRepository<TEntity>
    where TEntity : BaseEntity
{
    protected readonly BookHubDBContext _context;

    protected GenericRepository(BookHubDBContext context)
    {
        _context = context;
    }

    public virtual async Task<TEntity> Add(TEntity entity)
    {
        var result = await _context.Set<TEntity>().AddAsync(entity);
        
        await _context.SaveChangesAsync();

        return result.Entity;
    }
    
    public async Task<IEnumerable<TEntity>> GetAll()
    {
        return await _context.Set<TEntity>().ToListAsync();
    }

    public async Task<TEntity> Delete(TEntity entity)
    {
        _context.Set<TEntity>().Remove(entity);
        await _context.SaveChangesAsync();

        return entity;
    }

    public async Task<TEntity> Update(TEntity entity)
    {
        _context.Set<TEntity>().Update(entity);
        await _context.SaveChangesAsync();
        return entity;
    }

    public async Task<bool> Exists(int id)
    {
        return await _context.Set<TEntity>().AnyAsync(t => t.Id == id);
    }

    public async Task<IEnumerable<TEntity>> Filter(Predicate<TEntity> filter)
    {
        return await _context.Set<TEntity>().Where(t => filter(t)).ToListAsync();
    }

    public async Task<TEntity?> GetById(int id)
    {
        return await _context.Set<TEntity>().FindAsync(id);
    }
    
}