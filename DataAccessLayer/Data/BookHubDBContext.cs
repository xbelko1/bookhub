using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Data;

public class BookHubDBContext : IdentityUserContext<User, int>
{
    public DbSet<User> Users { get; set; }
    public DbSet<WishList> WishLists { get; set; }
    public DbSet<WishListItem> WishlistItems { get; set; }
    public DbSet<Book> Books { get; set; }
    public DbSet<Genre> Genres { get; set; }
    public DbSet<Publisher> Publishers { get; set; }
    public DbSet<Author> Authors { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<OrderItem> OrderItems { get; set; }
    public DbSet<Review> Reviews { get; set; }

    public BookHubDBContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Define the many-to-one relationship between WishList and User
        modelBuilder.Entity<WishList>()
            .HasOne(w => w.User)
            .WithMany(u => u.WishLists)
            .HasForeignKey(w => w.UserId);

        modelBuilder.Entity<WishList>()
            .HasMany(w => w.Items)
            .WithOne(wi => wi.WishList);

        modelBuilder.Entity<WishListItem>()
            .HasOne(wi => wi.Book)
            .WithMany()
            .HasForeignKey(wi => wi.BookId);
        
        modelBuilder.Entity<Genre>()
            .HasMany(g => g.Books)
            .WithOne(b => b.Genre)
            .HasForeignKey(b => b.GenreId);

        modelBuilder.Entity<Publisher>()
            .HasMany(g => g.Books)
            .WithOne(b => b.Publisher)
            .HasForeignKey(b => b.PublisherId);

        modelBuilder.Entity<Author>()
            .HasMany(a => a.Books)
            .WithOne(b => b.Author)
            .HasForeignKey(b => b.AuthorId);
        
        modelBuilder.Entity<Order>()
            .HasMany(o => o.OrderItems)
            .WithOne(i => i.Order)
            .HasForeignKey(i => i.OrderId);
        
        modelBuilder.Entity<User>()
            .HasMany(u => u.Orders)
            .WithOne(o => o.User)
            .HasForeignKey(o => o.UserId);
        
        modelBuilder.Entity<User>()
            .HasOne(u => u.CustomerInfo)
            .WithOne(c => c.User)
            .HasForeignKey<CustomerInfo>(c => c.UserId);
        
        modelBuilder.Entity<Review>()
            .HasOne(r => r.User)
            .WithMany(u => u.Reviews)
            .HasForeignKey(r => r.UserId);
        
        modelBuilder.Entity<Review>()
            .HasOne(r => r.Book)
            .WithMany(b => b.Reviews)
            .HasForeignKey(r => r.BookId);
        
        modelBuilder.Seed();
        
        base.OnModelCreating(modelBuilder);
    }
}
