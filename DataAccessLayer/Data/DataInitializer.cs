using DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Data;

public static class DataInitializer
{
    public static void Seed(this ModelBuilder modelBuilder)
    {
        var genres = PrepareGenreModels();
        var publishers = PreparePublisherModels();
        var authors = PrepareAuthorsModels();
        var books = PrepareBooksModels();
        var reviews = PrepareReviewsModels();

        modelBuilder.Entity<Genre>()
            .HasData(genres);

        modelBuilder.Entity<Publisher>()
            .HasData(publishers);

        modelBuilder.Entity<Author>()
            .HasData(authors);

        modelBuilder.Entity<Book>()
            .HasData(books);
        
        modelBuilder.Entity<Review>()
            .HasData(reviews);
    }

    private static IEnumerable<Genre> PrepareGenreModels()
    {
        var genres = new List<Genre>
        {
            new()
            {
                Id = 1,
                Name = "Sci-fi"
            },
            new()
            {
                Id = 2,
                Name = "Romantika"
            },
            new()
            {
                Id = 3,
                Name = "Detektivka"
            },
            new()
            {
                Id = 4,
                Name = "Fantasy"
            },
            new()
            {
                Id = 5,
                Name = "Horror"
            },
            new()
            {
                Id = 6,
                Name = "Biografie"
            },
            new()
            {
                Id = 7,
                Name = "Kuchařka"
            },
            new()
            {
                Id = 8,
                Name = "Historie"
            },
            new()
            {
                Id = 9,
                Name = "Komedie"
            },
            new()
            {
                Id = 10,
                Name = "Drama"
            }

        };
        
        return genres;
    }

    private static IEnumerable<Publisher> PreparePublisherModels()
    {
        var publishers = new List<Publisher>
        {
            new()
            {
                Id = 1,
                Name = "Albatros Media"
            },
            new()
            {
                Id = 2,
                Name = "Grada Publishing"
            },
            new()
            {
                Id = 3,
                Name = "Euromedia Group"
            },
            new()
            {
                Id = 4,
                Name = "Fragment"
            },
            new()
            {
                Id = 5,
                Name = "Odeon"
            },
            new()
            {
                Id = 6,
                Name = "Portál"
            },
            new()
            {
                Id = 7,
                Name = "Academia"
            },
            new()
            {
                Id = 8,
                Name = "Argo"
            },
            new()
            {
                Id = 9,
                Name = "Host"
            },
            new()
            {
                Id = 10,
                Name = "Mladá fronta"
            }
        };

        return publishers;
    }

    private static IEnumerable<Author> PrepareAuthorsModels()
    {
        var authors = new List<Author>
        {
            new()
            {
                Id = 1,
                FirstName = "George",
                LastName = "Orwell",
                Age = 46
            },
            new()
            {
                Id = 2,
                FirstName = "J.K.",
                LastName = "Rowling",
                Age = 57
            },
            new()
            {
                Id = 3,
                FirstName = "Agatha",
                LastName = "Christie",
                Age = 85
            },
            new()
            {
                Id = 4,
                FirstName = "Stephen",
                LastName = "King",
                Age = 74
            },
            new()
            {
                Id = 5,
                FirstName = "Harper",
                LastName = "Lee",
                Age = 89
            },
            new()
            {
                Id = 6,
                FirstName = "Mark",
                LastName = "Twain",
                Age = 74
            },
            new()
            {
                Id = 7,
                FirstName = "F. Scott",
                LastName = "Fitzgerald",
                Age = 44
            },
            new()
            {
                Id = 8,
                FirstName = "Herman",
                LastName = "Melville",
                Age = 72
            },
            new()
            {
                Id = 9,
                FirstName = "Charles",
                LastName = "Dickens",
                Age = 58
            },
            new()
            {
                Id = 10,
                FirstName = "Leo",
                LastName = "Tolstoy",
                Age = 82
            }
        };
        
        return authors;
    }
    
    private static IEnumerable<Book> PrepareBooksModels()
    {
        var books = new List<Book>
        { 
            new()
            {
                Id = 1,
                Name = "Válka s Mloky",
                PublishYear = 1936,
                Price = 12.99m,
                Description = "Surrealistický román Karla Čapka.",
                PageNumber = 320,
                GenreId = 1,
                AuthorId = 1,
                PublisherId = 1,
            },
            new()
            {
                Id = 2,
                Name = "Příběh inženýra lidských duší",
                PublishYear = 1931,
                Price = 14.99m,
                Description = "Jedno z děl Františka Langera.",
                PageNumber = 280,
                GenreId = 2,
                AuthorId = 2,
                PublisherId = 2,
            },
            new()
            {
                Id = 3,
                Name = "Hovory s TGM",
                PublishYear = 1960,
                Price = 11.99m,
                Description = "Sbírka rozhovorů s prezidentem Tomášem Garriguem Masarykem.",
                PageNumber = 240,
                GenreId = 3,
                AuthorId = 3,
                PublisherId = 3,
            },
            new()
            {
                Id = 4,
                Name = "Pantáta Bezoušek",
                PublishYear = 1937,
                Price = 10.99m,
                Description = "Dětská kniha Eduarda Petišky.",
                PageNumber = 180,
                GenreId = 4,
                AuthorId = 4,
                PublisherId = 4,
            },
            new()
            {
                Id = 5,
                Name = "Babička",
                PublishYear = 1855,
                Price = 9.99m,
                Description = "Klasické dílo Boženy Němcové.",
                PageNumber = 260,
                GenreId = 5,
                AuthorId = 5,
                PublisherId = 5,
            },
            new()
            {
                Id = 6,
                Name = "Svět podle Garpova",
                PublishYear = 1978,
                Price = 16.99m,
                Description = "Román Johna Irvinga.",
                PageNumber = 380,
                GenreId = 6,
                AuthorId = 6,
                PublisherId = 6,
            },
            new()
            {
                Id = 7,
                Name = "Na druhém břehu řeky Piedra seděla a plakala",
                PublishYear = 1994,
                Price = 15.99m,
                Description = "Kniha od brazilské spisovatelky Paulo Coelha.",
                PageNumber = 320,
                GenreId = 7,
                AuthorId = 7,
                PublisherId = 7,
            },
            new()
            {
                Id = 8,
                Name = "Příběh umučeného dítěte",
                PublishYear = 1983,
                Price = 18.99m,
                Description = "Pamětník Filipa Müllerka.",
                PageNumber = 240,
                GenreId = 8,
                AuthorId = 8,
                PublisherId = 8,
            },
            new()
            {
                Id = 9,
                Name = "Nevinnost",
                PublishYear = 1991,
                Price = 13.99m,
                Description = "Kniha od Ivana Klímy.",
                PageNumber = 290,
                GenreId = 9,
                AuthorId = 9,
                PublisherId = 9,
            },
            new()
            {
                Id = 10,
                Name = "Pouta",
                PublishYear = 2011,
                Price = 20.99m,
                Description = "Thriller od Dominika Dána.",
                PageNumber = 330,
                GenreId = 10,
                AuthorId = 10,
                PublisherId = 10,
            }
        };
        
        return books;
    }
    
    private static IEnumerable<Review> PrepareReviewsModels()
    {
        var reviews = new List<Review>
        {
            new()
            {
                Id = 1,
                Text = "Úžasná kniha! Velmi doporučuji.",
                Rating = 5,
                BookId = 1
            },
            new()
            {
                Id = 2,
                Text = "Skvělá detektivka, napínavý příběh.",
                Rating = 4.5,
                BookId = 2
            },
            new()
            {
                Id = 3,
                Text = "Překrásná klasika, stojí za přečtení.",
                Rating = 5,
                BookId = 3
            },
            new()
            {
                Id = 4,
                Text = "Strhující horor, mám noční můry!",
                Rating = 4,
                BookId = 4
            },
            new()
            {
                Id = 5,
                Text = "Doporučuji všem milovníkům fantasy.",
                Rating = 4.5,
                BookId = 5
            },
            new()
            {
                Id = 6,
                Text = "Krásný román, emotivní a dojemný.",
                Rating = 5,
                BookId = 6
            },
            new()
            {
                Id = 7,
                Text = "Inspirující kniha, plná moudrých rad.",
                Rating = 4.5,
                BookId = 7
            },
            new()
            {
                Id = 8,
                Text = "Silný příběh, který vás nechá nepohnuté.",
                Rating = 4.5,
                BookId = 8
            },
            new()
            {
                Id = 9,
                Text = "Skvělá komedie, smích a zábava.",
                Rating = 4,
                BookId = 9
            },
            new()
            {
                Id = 10,
                Text = "Napínavý thriller, nelze odložit!",
                Rating = 5,
                BookId = 10
            }
        };

        return reviews;
    }
}
