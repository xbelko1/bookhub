using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Infrastructure.Middleware.Logger;

public class LoggerMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<LoggerMiddleware> _logger;

    public LoggerMiddleware(RequestDelegate next, ILogger<LoggerMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    private async Task LogRequest(HttpContext context)
    {
        _logger.LogInformation($"Request: {context.Request.Method} {context.Request.Path}");
        LogHeaders(context.Request.Headers);
        context.Request.EnableBuffering();
        var content = new StreamReader(context.Request.Body);
        await LogBody(context.Request.ContentLength, context.Request.ContentType, content);
        context.Request.Body.Position = 0;
    }

    private void LogHeaders(IHeaderDictionary headers)
    {
        if (headers.Count == 0) return;

        var headersString = new StringBuilder($"Headers: {Environment.NewLine}");
        foreach (var header in headers)
        {
            headersString.AppendLine($"{header.Key}: {header.Value}");
        }

        _logger.LogInformation(headersString.ToString());
    }

    private async Task LogBody(long? length, string? contentType, StreamReader reader)
    {
        if (length == 0 || contentType == null) return;

        var bodyAsync = await reader.ReadToEndAsync();
        _logger.LogInformation($"Body: {Environment.NewLine}{bodyAsync}");
    }

    public async Task Invoke(HttpContext context)
    {
        await LogRequest(context);
        var originalResponseBody = context.Response.Body;
        using (var responseBody = new MemoryStream())
        {
            context.Response.Body = responseBody;
            await _next(context);
            await LogResponse(context, responseBody, originalResponseBody);
        }
    }

    private async Task LogResponse(HttpContext context, MemoryStream responseBody, Stream originalResponseBody)
    {
        _logger.LogInformation($"Response: {context.Response.StatusCode}");
        LogHeaders(context.Response.Headers);
        responseBody.Position = 0;
        var content = new StreamReader(responseBody);
        await LogBody(context.Response.ContentLength, context.Response.ContentType, content);
        responseBody.Position = 0;
        await responseBody.CopyToAsync(originalResponseBody);
        context.Response.Body = originalResponseBody;
    }
}