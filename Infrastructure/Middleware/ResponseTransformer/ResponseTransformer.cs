using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;

namespace Infrastructure.Middleware.ResponseTransformer;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

public class ResponseTransformer
{
    private readonly RequestDelegate _next;

    public ResponseTransformer(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context)
    { 
        var responseFormat = context.Request.Query["responseFormat"].ToString().ToLower();;

        if (responseFormat == "xml")
        {
            await HandleXmlResponseAsync(context);
        }
        else
        {
            await _next(context);
        }
    }

    private async Task HandleXmlResponseAsync(HttpContext context)
    {
        var originalBodyStream = context.Response.Body;

        using var responseBody = new MemoryStream();
        context.Response.Body = responseBody;

        await _next(context);

        context.Response.Body.Seek(0, SeekOrigin.Begin);
        var json = await new StreamReader(context.Response.Body).ReadToEndAsync();

        context.Response.ContentType = "text/xml";
        XmlDocument? xmlDoc = null;
        if (json.Length > 0)
        {
            xmlDoc = ConvertToJsonToXmlAsync(json);
        }
        context.Response.Body = originalBodyStream;
        await context.Response.WriteAsync(xmlDoc?.OuterXml ?? "");
    } 
    
    private XmlDocument ConvertToJsonToXmlAsync(string json)
    {
        string wrappedJson = $"{{ Item: {json} }}";
        XmlDocument? xmlDoc = JsonConvert.DeserializeXmlNode(wrappedJson, "Response");
        if (xmlDoc != null)
        {
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement? root = xmlDoc.DocumentElement;
            xmlDoc.InsertBefore(xmlDeclaration, root);
            return xmlDoc;
        }

        
        throw new Exception("Cannot Parse JSON TO XML");
    }

}

