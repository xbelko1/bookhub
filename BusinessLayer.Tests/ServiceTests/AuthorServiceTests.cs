using AutoMapper;
using BusinessLayer.DTOs.Author;
using BusinessLayer.Enums;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using NSubstitute;

namespace BusinessLayer.Tests.ServiceTests;

public class AuthorServiceTests
{
    [Fact]
    public async Task AddAuthor_Successful_Result()
    {
        var authorRepository = Substitute.For<IAuthorRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new AuthorService(authorRepository, mapper);

        var authorCreateDto = new AuthorCreateDto { FirstName = "John", LastName = "Doe", Age = 30 };
        var authorEntity = new Author { Id = 1, FirstName = "John", LastName = "Doe", Age = 30 };
        var authorDto = new AuthorDto { Id = 1, FirstName = "John", LastName = "Doe", Age = 30 };

        authorRepository.Add(Arg.Any<Author>()).Returns(authorEntity);
        mapper.Map<Author>(authorCreateDto).Returns(authorEntity);
        mapper.Map<AuthorDto>(authorEntity).Returns(authorDto);

        var result = await service.AddAuthor(authorCreateDto);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(authorDto, result.Data);
    }
    
    [Fact]
    public async Task DeleteAuthor_Successful_Result()
    {
        var authorRepository = Substitute.For<IAuthorRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new AuthorService(authorRepository, mapper);

        const int authorId = 1;
        var authorEntity = new Author { Id = authorId, FirstName = "John", LastName = "Doe", Age = 30 };
        var authorDto = new AuthorDto { Id = authorId, FirstName = "John", LastName = "Doe", Age = 30 };

        authorRepository.GetById(authorId).Returns(authorEntity);
        authorRepository.Delete(Arg.Any<Author>()).Returns(authorEntity);
        mapper.Map<AuthorDto>(authorEntity).Returns(authorDto);

        var result = await service.DeleteAuthor(authorId);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(authorDto, result.Data);
    }
    
    [Fact]
    public async Task DeleteAuthor_Failure_Result_When_Author_Not_Exists()
    {
        var authorRepository = Substitute.For<IAuthorRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new AuthorService(authorRepository, mapper);

        const string errorMessage = "Author not found";
        const int authorId = 1;
        authorRepository.GetById(authorId).Returns(Task.FromResult<Author?>(null));

        var result = await service.DeleteAuthor(authorId);

        Assert.Equal(ResultCode.NotFound, result.ResultCode);
        Assert.Equal(errorMessage, result.ErrorMessage);
        Assert.Null(result.Data);
    }
    
    [Fact]
    public async Task UpdateAuthor_Successful_Result_When_Author_Exists()
    {
        var authorRepository = Substitute.For<IAuthorRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new AuthorService(authorRepository, mapper);
        const int authorId = 1;
        var updateAuthorDto = new AuthorCreateDto { FirstName = "John", LastName = "Doe", Age = 35 };
        var authorEntity = new Author { Id = authorId, FirstName = "John", LastName = "Doe", Age = 30 };
        var updatedAuthorDto = new AuthorDto { Id = authorId, FirstName = "John", LastName = "Doe", Age = 35 };

        authorRepository.GetById(authorId).Returns(authorEntity);
        mapper.Map<AuthorDto>(authorEntity).Returns(updatedAuthorDto);

        var result = await service.UpdateAuthor(authorId, updateAuthorDto);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(updatedAuthorDto, result.Data);
        await authorRepository.Received(1).Update(Arg.Any<Author>());
    }

    [Fact]
    public async Task UpdateAuthor_Failure_Result_When_Author_Not_Exists()
    {
        var authorRepository = Substitute.For<IAuthorRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new AuthorService(authorRepository, mapper);
        const string errorMessage = "Author not found";
        const int authorId = 1;
        var updateAuthorDto = new AuthorCreateDto { FirstName = "John", LastName = "Doe", Age = 35 };

        authorRepository.GetById(authorId).Returns(Task.FromResult<Author?>(null));

        var result = await service.UpdateAuthor(authorId, updateAuthorDto);

        Assert.Equal(ResultCode.NotFound, result.ResultCode);
        Assert.Equal(errorMessage, result.ErrorMessage);
        Assert.Null(result.Data);
    }
    
    [Fact]
    public async Task GetAllAuthors_Successful_Result_With_Authors()
    {
        var authorRepository = Substitute.For<IAuthorRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new AuthorService(authorRepository, mapper);

        var authorEntities = new List<Author>
        {
            new() { Id = 1, FirstName = "John", LastName = "Doe", Age = 30 },
            new() { Id = 2, FirstName = "Jane", LastName = "Smith", Age = 40 }
        };

        var authorDtos = new List<AuthorDto>
        {
            new() { Id = 1, FirstName = "John", LastName = "Doe", Age = 30 },
            new() { Id = 2, FirstName = "Jane", LastName = "Smith", Age = 40 }
        };

        authorRepository.GetAll().Returns(authorEntities);
        mapper.Map<IEnumerable<AuthorDto>>(authorEntities).Returns(authorDtos);

        var result = await service.GetAllAuthors();

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(authorDtos, result.Data);
    }

    [Fact]
    public async Task GetAuthorById_Successful_Result_When_Author_Exists()
    {
        var authorRepository = Substitute.For<IAuthorRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new AuthorService(authorRepository, mapper);

        const int authorId = 1;
        var authorEntity = new Author { Id = authorId, FirstName = "John", LastName = "Doe", Age = 30 };
        var authorDto = new AuthorDto { Id = authorId, FirstName = "John", LastName = "Doe", Age = 30 };

        authorRepository.GetById(authorId).Returns(authorEntity);
        mapper.Map<AuthorDto>(authorEntity).Returns(authorDto);

        var result = await service.GetAuthorById(authorId);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(authorDto, result.Data);
    }

    [Fact]
    public async Task GetAuthorById_Failure_Result_When_Author_Not_Exists()
    {
        var authorRepository = Substitute.For<IAuthorRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new AuthorService(authorRepository, mapper);

        const int authorId = 1;
        const string errorMessage = "Author not found";
        authorRepository.GetById(authorId).Returns(Task.FromResult<Author?>(null));

        var result = await service.GetAuthorById(authorId);

        Assert.Equal(ResultCode.NotFound, result.ResultCode);
        Assert.Equal(errorMessage, result.ErrorMessage);
        Assert.Null(result.Data);
    }
}
