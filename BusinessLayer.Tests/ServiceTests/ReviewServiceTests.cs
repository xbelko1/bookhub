using AutoMapper;
using BusinessLayer.DTOs.Review;
using BusinessLayer.Enums;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using NSubstitute;

namespace BusinessLayer.Tests.ServiceTests;

public class ReviewServiceTests
{
    [Fact]
    public async Task AddReview_Successful_Result()
    {
        var reviewRepository = Substitute.For<IReviewRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new ReviewService(reviewRepository, mapper);

        const int userId = 1;
        var reviewCreateDto = new ReviewCreateDto { Text = "Test text", Rating = 10, BookId = 1};
        var reviewEntity = new Review { Id = 1, Text = "Test text", BookId = 1, Rating = 10, UserId = userId};
        var reviewDto = new ReviewDto { Id = 1, Text = "Test text", BookId = 1, Rating = 10};

        reviewRepository.Add(Arg.Any<Review>()).Returns(reviewEntity);
        mapper.Map<Review>(reviewCreateDto).Returns(reviewEntity);
        mapper.Map<ReviewDto>(reviewEntity).Returns(reviewDto);
        
        var result = await service.AddReview(userId, reviewCreateDto);
        
        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(reviewDto, result.Data);
    }
    
    [Fact]
    public async Task AddReview_Failure_Result_When_Rating_Is_Incorrect()
    {
        var reviewRepository = Substitute.For<IReviewRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new ReviewService(reviewRepository, mapper);

        const int userId = 1;
        var reviewCreateDto = new ReviewCreateDto { Text = "Test text", Rating = -2, BookId = 1};
        var reviewCreateDto2 = new ReviewCreateDto { Text = "Test text", Rating = 11, BookId = 1};

        const string errorMessage = "Rating is incorrect";
        
        var result = await service.AddReview(userId, reviewCreateDto);
        
        Assert.Equal(ResultCode.BadRequest, result.ResultCode);
        Assert.Equal(errorMessage, result.ErrorMessage);
        Assert.Null(result.Data);
        
        var result2 = await service.AddReview(userId, reviewCreateDto2);
        
        Assert.Equal(ResultCode.BadRequest, result2.ResultCode);
        Assert.Equal(errorMessage, result2.ErrorMessage);
        Assert.Null(result2.Data);
    }
    
    [Fact]
    public async Task DeleteReview_Successful_Result()
    {
        var reviewRepository = Substitute.For<IReviewRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new ReviewService(reviewRepository, mapper);

        const int reviewId = 1;
        var reviewEntity = new Review { Id = reviewId, Text = "Test text", BookId = 1, Rating = 10, UserId = 1};
        var reviewDto = new ReviewDto { Id = reviewId, Text = "Test text", BookId = 1, Rating = 10};
        
        reviewRepository.GetById(reviewId).Returns(reviewEntity);
        mapper.Map<ReviewDto>(reviewEntity).Returns(reviewDto);

        var result = await service.DeleteReview(reviewId);
        
        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(reviewDto, result.Data);
    }

    [Fact]
    public async Task DeleteReview_Failure_Result_When_Review_Not_Exists()
    {
        var reviewRepository = Substitute.For<IReviewRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new ReviewService(reviewRepository, mapper);

        const string errorMessage = "Review not found";
        const int reviewId = 1;
        reviewRepository.GetById(reviewId).Returns(Task.FromResult<Review?>(null));
        
        var result = await service.DeleteReview(reviewId);
        
        Assert.Equal(ResultCode.NotFound, result.ResultCode);
        Assert.Equal(errorMessage, result.ErrorMessage);
        Assert.Null(result.Data);
    }
    
    [Fact]
    public async Task UpdateReview_Successful_Result_When_Review_Exists()
    {
        var reviewRepository = Substitute.For<IReviewRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new ReviewService(reviewRepository, mapper);
        const int reviewId = 1;
        var reviewCreateDto = new ReviewCreateDto { Text = "Test text updated", Rating = 10, BookId = 1};
        var reviewEntity = new Review { Id = reviewId, Text = "Test text", BookId = 1, Rating = 10, UserId = 1};
        var reviewUpdatedDto = new ReviewDto { Id = reviewId, Text = "Test text updated", BookId = 1, Rating = 10};
        
        reviewRepository.GetById(reviewId).Returns(reviewEntity);
        mapper.Map<ReviewDto>(reviewEntity).Returns(reviewUpdatedDto);

        var result = await service.UpdateReview(reviewId, reviewCreateDto);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(reviewUpdatedDto, result.Data);
        await reviewRepository.Received(1).Update(Arg.Any<Review>());
    }
    
    [Fact]
    public async Task UpdateReview_Failure_Result_When_Review_Not_Exists()
    {
        var reviewRepository = Substitute.For<IReviewRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new ReviewService(reviewRepository, mapper);
        const string errorMessage = "Review not found";
        const int reviewId = 1;
        var reviewCreateDto = new ReviewCreateDto { Text = "Test text updated", Rating = 10, BookId = 1};

        reviewRepository.GetById(reviewId).Returns(Task.FromResult<Review?>(null));
        
        var result = await service.UpdateReview(reviewId, reviewCreateDto);

        Assert.Equal(ResultCode.NotFound, result.ResultCode);
        Assert.Equal(errorMessage, result.ErrorMessage);
        Assert.Null(result.Data);
    }

    [Fact]
    public async Task UpdateReview_Failure_Result_When_Rating_Is_Incorrect()
    {
        var reviewRepository = Substitute.For<IReviewRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new ReviewService(reviewRepository, mapper);

        const int reviewId = 1;
        var reviewCreateDto = new ReviewCreateDto { Text = "Test text", Rating = -2, BookId = 1 };
        var reviewCreateDto2 = new ReviewCreateDto { Text = "Test text", Rating = 11, BookId = 1 };

        const string errorMessage = "Rating is incorrect";

        var result = await service.UpdateReview(reviewId, reviewCreateDto);

        Assert.Equal(ResultCode.BadRequest, result.ResultCode);
        Assert.Equal(errorMessage, result.ErrorMessage);
        Assert.Null(result.Data);

        var result2 = await service.UpdateReview(reviewId, reviewCreateDto2);

        Assert.Equal(ResultCode.BadRequest, result2.ResultCode);
        Assert.Equal(errorMessage, result2.ErrorMessage);
        Assert.Null(result2.Data);
    }

    [Fact]
    public async Task GetAllReviews_Successful_Result_With_Reviews()
    {
        var reviewRepository = Substitute.For<IReviewRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new ReviewService(reviewRepository, mapper);

        var reviewEntities = new List<Review>
        {
            new() { Id = 1, Text = "Test text", BookId = 1, Rating = 10, UserId = 1},
            new() { Id = 2, Text = "Test text2", BookId = 2, Rating = 5, UserId = 3}
        };

        var reviewDtos = new List<ReviewDto>
        {
            new() { Id = 1, Text = "Test text", BookId = 1, Rating = 10},
            new() { Id = 2, Text = "Test text2", BookId = 2, Rating = 5}
        };
        reviewRepository.GetAll().Returns(reviewEntities);
        mapper.Map<IEnumerable<ReviewDto>>(reviewEntities).Returns(reviewDtos);

        var result = await service.GetAllReviews();

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(reviewDtos, result.Data);
    }
    
    [Fact]
    public async Task GetReviewById_Successful_Result_When_Review_Exists()
    {
        var reviewRepository = Substitute.For<IReviewRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new ReviewService(reviewRepository, mapper);

        const int reviewId = 1;
        var reviewEntity = new Review { Id = reviewId, Text = "Test text", BookId = 1, Rating = 10, UserId = 1};
        var reviewDto = new ReviewDto { Id = reviewId, Text = "Test text", BookId = 1, Rating = 10};

        reviewRepository.GetById(reviewId).Returns(reviewEntity);
        mapper.Map<ReviewDto>(reviewEntity).Returns(reviewDto);

        var result = await service.GetReviewById(reviewId);
        
        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(reviewDto, result.Data);
    }
    
    [Fact]
    public async Task GetReviewById_Failure_Result_When_Review_Not_Exists()
    {
        var reviewRepository = Substitute.For<IReviewRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new ReviewService(reviewRepository, mapper);

        const int reviewId = 1;
        const string errorMessage = "Review not found";
        reviewRepository.GetById(reviewId).Returns(Task.FromResult<Review?>(null));

        var result = await service.GetReviewById(reviewId);
        
        Assert.Equal(ResultCode.NotFound, result.ResultCode);
        Assert.Equal(errorMessage, result.ErrorMessage);
        Assert.Null(result.Data);
    }
}
