using AutoMapper;
using BusinessLayer.DTOs.Book;
using BusinessLayer.Enums;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using NSubstitute;


namespace BusinessLayer.Tests.ServiceTests;

public class BookServiceTests
{
    [Fact]
    public async Task AddBook_Successful_Result()
    {
        var bookRepository = Substitute.For<IBookRepository>();
        var genreRepository = Substitute.For<IGenreRepository>();
        var publisherRepository = Substitute.For<IPublisherRepository>();
        var authorRepository = Substitute.For<IAuthorRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new BookService(bookRepository, genreRepository, publisherRepository, authorRepository, mapper);

        var bookCreateDto = new BookCreateDto
        {
            GenreId = 1,
            PublisherId = 1,
            AuthorId = 1
        };

        genreRepository.Exists(1).Returns(true);
        publisherRepository.Exists(1).Returns(true);
        authorRepository.Exists(1).Returns(true);

        var addedBook = new Book { Id = 1 };
        bookRepository.Add(Arg.Any<Book>()).Returns(addedBook);
        mapper.Map<Book>(bookCreateDto).Returns(addedBook);
        mapper.Map<BookDto>(addedBook).Returns(new BookDto { Id = 1 });

        var result = await service.AddBook(bookCreateDto);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(1, result.Data!.Id);
    }

    [Fact]
    public async Task AddBook_Failure_Result_When_Genre_Not_Found()
    {
        var bookRepository = Substitute.For<IBookRepository>();
        var genreRepository = Substitute.For<IGenreRepository>();
        var publisherRepository = Substitute.For<IPublisherRepository>();
        var authorRepository = Substitute.For<IAuthorRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new BookService(bookRepository, genreRepository, publisherRepository, authorRepository, mapper);

        var bookCreateDto = new BookCreateDto
        {
            GenreId = 1,
            PublisherId = 1,
            AuthorId = 1
        };

        genreRepository.Exists(1).Returns(false);

        var result = await service.AddBook(bookCreateDto);

        Assert.Equal(ResultCode.NotFound, result.ResultCode);
        Assert.Equal("Publisher, Genre or Author not found", result.ErrorMessage);
        Assert.Null(result.Data);
    }
    
    [Fact]
    public async Task UpdateBook_Successful_Result_When_Book_Exists()
    {
        var bookRepository = Substitute.For<IBookRepository>();
        var genreRepository = Substitute.For<IGenreRepository>();
        var publisherRepository = Substitute.For<IPublisherRepository>();
        var authorRepository = Substitute.For<IAuthorRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new BookService(bookRepository, genreRepository, publisherRepository, authorRepository, mapper);

        var updatedBookCreateDto = new BookCreateDto
        {
            GenreId = 2,
            PublisherId = 2,
            AuthorId = 2
        };

        var bookEntity = new Book { Id = 1, PageNumber = 100, PublishYear = 2013 };
        var updatedBookDto = new BookDto { Id = 1, PageNumber = 200, PublishYear = 2016 };

        bookRepository.GetById(1).Returns(bookEntity);
        genreRepository.Exists(2).Returns(true);
        publisherRepository.Exists(2).Returns(true);
        authorRepository.Exists(2).Returns(true);

        mapper.Map<BookDto>(bookEntity).Returns(updatedBookDto);

        var result = await service.UpdateBook(1, updatedBookCreateDto);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(200, result.Data!.PageNumber);
        Assert.Equal(2016, result.Data.PublishYear);
    }

    [Fact]
    public async Task DeleteBook_Failure_Result_When_Book_Not_Found()
    {
        var bookRepository = Substitute.For<IBookRepository>();
        var genreRepository = Substitute.For<IGenreRepository>();
        var publisherRepository = Substitute.For<IPublisherRepository>();
        var authorRepository = Substitute.For<IAuthorRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new BookService(bookRepository, genreRepository, publisherRepository, authorRepository, mapper);

        bookRepository.GetById(1).Returns(Task.FromResult<Book>(null));

        var result = await service.DeleteBook(1);

        Assert.Equal(ResultCode.NotFound, result.ResultCode);
        Assert.Equal("Book not found", result.ErrorMessage);
        Assert.Null(result.Data);
    }

    [Fact]
    public async Task GetBookById_Failure_Result_When_Book_Not_Found()
    {
        var bookRepository = Substitute.For<IBookRepository>();
        var genreRepository = Substitute.For<IGenreRepository>();
        var publisherRepository = Substitute.For<IPublisherRepository>();
        var authorRepository = Substitute.For<IAuthorRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new BookService(bookRepository, genreRepository, publisherRepository, authorRepository, mapper);

        bookRepository.GetById(1).Returns(Task.FromResult<Book>(null));

        var result = await service.GetBookById(1);

        Assert.Equal(ResultCode.NotFound, result.ResultCode);
        Assert.Equal("Book not found", result.ErrorMessage);
        Assert.Null(result.Data);
    }
}
