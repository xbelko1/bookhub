using AutoMapper;
using BusinessLayer.DTOs.Genre;
using BusinessLayer.Enums;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using NSubstitute;

namespace BusinessLayer.Tests.ServiceTests;

public class GenreServiceTests
{
    [Fact]
    public async Task AddGenre_Successful_Result()
    {
        var genreRepository = Substitute.For<IGenreRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new GenreService(genreRepository, mapper);

        var genreCreateDto = new GenreCreateDto { Name = "TestGenre" };
        var addedGenre = new Genre { Id = 1, Name = "TestGenre" };
        var genreDto = new GenreDto { Id = 1, Name = "TestGenre" };

        genreRepository.Add(Arg.Any<Genre>()).Returns(addedGenre);
        mapper.Map<Genre>(genreCreateDto).Returns(addedGenre);
        mapper.Map<GenreDto>(addedGenre).Returns(genreDto);

        var result = await service.AddGenre(genreCreateDto);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(genreDto, result.Data);
    }

    [Fact]
    public async Task GetGenreById_Successful_Result_When_Genre_Exists()
    {
        var genreRepository = Substitute.For<IGenreRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new GenreService(genreRepository, mapper);

        var genreEntity = new Genre { Id = 1, Name = "TestGenre" };
        var genreDto = new GenreDto { Id = 1, Name = "TestGenre" };

        genreRepository.GetById(1).Returns(genreEntity);
        mapper.Map<GenreDto>(genreEntity).Returns(genreDto);

        var result = await service.GetGenreById(1);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(genreDto, result.Data);
    }

    [Fact]
    public async Task GetAllGenres_Successful_Result_With_Genres()
    {
        var genreRepository = Substitute.For<IGenreRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new GenreService(genreRepository, mapper);

        var genreEntities = new List<Genre>
        {
            new() { Id = 1, Name = "Genre1" },
            new() { Id = 2, Name = "Genre2" }
        };

        var genreDtos = new List<GenreDto>
        {
            new() { Id = 1, Name = "Genre1" },
            new() { Id = 2, Name = "Genre2" }
        };

        genreRepository.GetAll().Returns(genreEntities);
        mapper.Map<IEnumerable<GenreDto>>(genreEntities).Returns(genreDtos);

        var result = await service.GetAllGenres();

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(genreDtos, result.Data);
    }

    [Fact]
    public async Task DeleteGenre_Successful_Result_When_Genre_Exists()
    {
        var genreRepository = Substitute.For<IGenreRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new GenreService(genreRepository, mapper);

        var genreEntity = new Genre { Id = 1, Name = "TestGenre" };
        var genreDto = new GenreDto { Id = 1, Name = "TestGenre" };

        genreRepository.GetById(1).Returns(genreEntity);
        mapper.Map<GenreDto>(genreEntity).Returns(genreDto);

        var result = await service.DeleteGenre(1);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(genreDto, result.Data);
        await genreRepository.Received(1).Delete(genreEntity);
    }

    [Fact]
    public async Task UpdateGenre_Successful_Result_When_Genre_Exists()
    {
        var genreRepository = Substitute.For<IGenreRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new GenreService(genreRepository, mapper);

        var genreEntity = new Genre { Id = 1, Name = "TestGenre" };
        var genreCreateDto = new GenreCreateDto { Name = "UpdatedGenre" };
        var updatedGenreDto = new GenreDto { Id = 1, Name = "UpdatedGenre" };

        genreRepository.GetById(1).Returns(genreEntity);
        mapper.Map<GenreDto>(genreEntity).Returns(updatedGenreDto);

        var result = await service.UpdateGenre(1, genreCreateDto);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(updatedGenreDto, result.Data);
        await genreRepository.Received(1).Update(Arg.Any<Genre>());
    }
}