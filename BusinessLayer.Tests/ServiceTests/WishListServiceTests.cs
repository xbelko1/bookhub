using AutoMapper;
using BusinessLayer.DTOs.WishList;
using BusinessLayer.Enums;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using NSubstitute;

namespace BusinessLayer.Tests.ServiceTests;

public class WishListServiceTests
{
    [Fact]
    public async Task AddWishList_Successful_Result()
    {
        var wishListRepository = Substitute.For<IWishListRepository>();
        var mapper = Substitute.For<IMapper>();
        var bookRepository = Substitute.For<IBookRepository>();

        var service = new WishListService(wishListRepository, mapper, bookRepository);

        const int userId = 1;
        var wishListCreateDto = new WistListCreateDto { Name = "My WishList" };

        var addedWishList = new WishList { Id = 1, Name = "My WishList", UserId = userId };
        wishListRepository.Add(Arg.Any<WishList>()).Returns(addedWishList);
        mapper.Map<WishListDto>(addedWishList).Returns(new WishListDto { Id = 1, Name = "My WishList", UserId = userId });

        var result = await service.AddWishList(userId, wishListCreateDto);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(1, result.Data.Id);
    }

    [Fact]
    public async Task GetWishListById_Successful_Result_When_WishList_Exists()
    {
        var wishListRepository = Substitute.For<IWishListRepository>();
        var mapper = Substitute.For<IMapper>();
        var bookRepository = Substitute.For<IBookRepository>();

        var service = new WishListService(wishListRepository, mapper, bookRepository);

        const int wishListId = 1;
        var wishListEntity = new WishList { Id = 1, Name = "My WishList", UserId = 1 };

        wishListRepository.GetById(wishListId).Returns(wishListEntity);
        mapper.Map<WishListDto>(wishListEntity).Returns(new WishListDto { Id = 1, Name = "My WishList", UserId = 1 });

        var result = await service.GetWishListById(wishListId);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(wishListId, result.Data.Id);
    }

    [Fact]
    public async Task GetAllWishLists_Successful_Result_With_WishLists()
    {
        var wishListRepository = Substitute.For<IWishListRepository>();
        var mapper = Substitute.For<IMapper>();
        var bookRepository = Substitute.For<IBookRepository>();

        var service = new WishListService(wishListRepository, mapper, bookRepository);

        var wishListEntities = new List<WishList>
        {
            new() { Id = 1, Name = "WishList 1", UserId = 1 },
            new() { Id = 2, Name = "WishList 2", UserId = 1 }
        };

        wishListRepository.GetAll().Returns(wishListEntities);
        mapper.Map<List<WishListDto>>(wishListEntities).Returns(new List<WishListDto>
        {
            new() { Id = 1, Name = "WishList 1", UserId = 1 },
            new() { Id = 2, Name = "WishList 2", UserId = 1 }
        });

        var result = await service.GetAllWishLists();

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(2, result.Data.Count());
    }

    [Fact]
    public async Task DeleteBookFromWishList_Successful_Result_When_Book_Exists_In_WishList()
    {
        var wishListRepository = Substitute.For<IWishListRepository>();
        var mapper = Substitute.For<IMapper>();
        var bookRepository = Substitute.For<IBookRepository>();

        var service = new WishListService(wishListRepository, mapper, bookRepository);

        const int wishListId = 1;
        const int bookId = 2;

        var wishListEntity = new WishList { Id = 1, Name = "My WishList", UserId = 1, Items = new List<WishListItem> { new() { BookId = bookId } } };

        wishListRepository.GetById(wishListId).Returns(wishListEntity);
        wishListRepository.Update(Arg.Any<WishList>()).Returns(wishListEntity);
        mapper.Map<WishListDto>(wishListEntity).Returns(new WishListDto { Id = 1, Name = "My WishList", UserId = 1 });

        var result = await service.DeleteBookFromWishList(wishListId, bookId);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(wishListId, result.Data.Id);
        Assert.Empty(result.Data.Items);
    }
}