﻿using AutoMapper;
using BusinessLayer.DTOs.Publisher;
using BusinessLayer.Enums;
using BusinessLayer.Services;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using NSubstitute;

namespace BusinessLayer.Tests.ServiceTests;

public class PublisherServiceTests
{
    [Fact]
    public async Task AddPublisher_Successful_Result()
    {
        var publisherRepository = Substitute.For<IPublisherRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new PublisherService(publisherRepository, mapper);

        var publisherCreateDto = new PublisherCreateDto { Name = "TestPublisher" };
        var publisherEntity = new Publisher { Id = 1, Name = "TestPublisher" };
        var publisherDto = new PublisherDto { Id = 1, Name = "TestPublisher" };

        publisherRepository.Add(Arg.Any<Publisher>()).Returns(publisherEntity);
        mapper.Map<Publisher>(publisherCreateDto).Returns(publisherEntity);
        mapper.Map<PublisherDto>(publisherEntity).Returns(publisherDto);
        
        var result = await service.AddPublisher(publisherCreateDto);
        
        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(publisherDto, result.Data);
    }

    [Fact]
    public async Task GetPublisherById_Successful_Result_When_Publisher_Exists()
    {
        var publisherRepository = Substitute.For<IPublisherRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new PublisherService(publisherRepository, mapper);

        var publisherEntity = new Publisher { Id = 1, Name = "TestPublisher" };
        var publisherDto = new PublisherDto { Id = 1, Name = "TestPublisher" };

        publisherRepository.GetById(1).Returns(publisherEntity);
        mapper.Map<PublisherDto>(publisherEntity).Returns(publisherDto);

        var result = await service.GetPublisherById(1);
        
        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(publisherDto, result.Data);
    }
    
    [Fact]
    public async Task GetAllPublishers_Successful_Result_With_Publishers()
    {
        var publisherRepository = Substitute.For<IPublisherRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new PublisherService(publisherRepository, mapper);

        var publisherEntities = new List<Publisher>
        {
            new() { Id = 1, Name = "Publisher1" },
            new() { Id = 2, Name = "Publisher2" }
        };

        var publisherDtOs = new List<PublisherDto>
        {
            new() { Id = 1, Name = "Publisher1" },
            new() { Id = 2, Name = "Publisher2" }
        };
        publisherRepository.GetAll().Returns(publisherEntities);
        mapper.Map<IEnumerable<PublisherDto>>(publisherEntities).Returns(publisherDtOs);

        var result = await service.GetAllPublishers();

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(publisherDtOs, result.Data);
    }

    [Fact]
    public async Task DeletePublisher_Successful_Result_When_Publisher_Exists()
    {
        var publisherRepository = Substitute.For<IPublisherRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new PublisherService(publisherRepository, mapper);

        var publisherEntity = new Publisher { Id = 1, Name = "TestPublisher" };
        var publisherDto = new PublisherDto { Id = 1, Name = "TestPublisher" };

        publisherRepository.GetById(1).Returns(publisherEntity);
        mapper.Map<PublisherDto>(publisherEntity).Returns(publisherDto);

        var result = await service.DeletePublisher(1);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(publisherDto, result.Data);
        await publisherRepository.Received(1).Delete(publisherEntity);
    }

    [Fact]
    public async Task UpdatePublisher_Successful_Result_When_Publisher_Exists()
    {
        var publisherRepository = Substitute.For<IPublisherRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new PublisherService(publisherRepository, mapper);

        var publisherEntity = new Publisher { Id = 1, Name = "TestPublisher" };
        var publisherCreateDto = new PublisherCreateDto { Name = "UpdatedPublisher" };
        var updatedPublisherDto = new PublisherDto { Id = 1, Name = "UpdatedPublisher" };

        publisherRepository.GetById(1).Returns(publisherEntity);
        mapper.Map<PublisherDto>(publisherEntity).Returns(updatedPublisherDto);

        var result = await service.UpdatePublisher(1, publisherCreateDto);

        Assert.Equal(ResultCode.Ok, result.ResultCode);
        Assert.Equal(updatedPublisherDto, result.Data);
        await publisherRepository.Received(1).Update(Arg.Any<Publisher>());
    }
    
    [Fact]
    public async Task GetPublisherById_Failure_Result_When_Publisher_Not_Exists()
    {
        var publisherRepository = Substitute.For<IPublisherRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new PublisherService(publisherRepository, mapper);

        publisherRepository.GetById(1).Returns((Publisher)null);

        var result = await service.GetPublisherById(1);

        Assert.Equal(ResultCode.NotFound, result.ResultCode);
        Assert.Null(result.Data);
    }

    [Fact]
    public async Task DeletePublisher_Failure_Result_When_Publisher_Not_Exists()
    {
        var publisherRepository = Substitute.For<IPublisherRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new PublisherService(publisherRepository, mapper);

        publisherRepository.GetById(1).Returns((Publisher)null);

        var result = await service.DeletePublisher(1);

        Assert.Equal(ResultCode.NotFound, result.ResultCode);
        Assert.Null(result.Data);
        await publisherRepository.DidNotReceive().Delete(Arg.Any<Publisher>());
    }

    [Fact]
    public async Task UpdatePublisher_Failure_Result_When_Publisher_Not_Exists()
    {
        var publisherRepository = Substitute.For<IPublisherRepository>();
        var mapper = Substitute.For<IMapper>();

        var service = new PublisherService(publisherRepository, mapper);

        publisherRepository.GetById(1).Returns((Publisher)null);

        var result = await service.UpdatePublisher(1, new PublisherCreateDto { Name = "UpdatedPublisher" });

        Assert.Equal(ResultCode.NotFound, result.ResultCode);
        Assert.Null(result.Data);
        await publisherRepository.DidNotReceive().Update(Arg.Any<Publisher>());
    }
}
