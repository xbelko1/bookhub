using AutoMapper;
using BusinessLayer.DTOs.Author;
using DataAccessLayer.Entities;

namespace BusinessLayer.Mapper;

public class AuthorMapper : Profile
{
    public AuthorMapper()
    {
        CreateMap<AuthorCreateDto, Author>();
        CreateMap<Author, AuthorDto>();
    }
}
