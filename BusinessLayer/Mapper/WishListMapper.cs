using AutoMapper;
using BusinessLayer.DTOs.WishList;
using DataAccessLayer.Entities;

namespace BusinessLayer.Mapper;

public class WishListMapper : Profile
{
    public WishListMapper()
    {
        CreateMap<WistListCreateDto, WishList>();
        CreateMap<WishList, WishListDto>();
    }
}