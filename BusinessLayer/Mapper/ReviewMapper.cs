using AutoMapper;
using BusinessLayer.DTOs.Review;
using DataAccessLayer.Entities;

namespace BusinessLayer.Mapper;

public class ReviewMapper : Profile
{
    public ReviewMapper()
    {
        CreateMap<ReviewCreateDto, Review>();
        CreateMap<Review, ReviewDto>();
        CreateMap<Review, ReviewCreateDto>();
    }
}