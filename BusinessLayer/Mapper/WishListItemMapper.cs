using AutoMapper;
using BusinessLayer.DTOs.WishListItem;
using DataAccessLayer.Entities;

namespace BusinessLayer.Mapper;

public class WishListItemMapper : Profile
{
    public WishListItemMapper()
    {
        CreateMap<WishListItem, WishListItemDto>();
        CreateMap<WishListItemDto, WishListItem>();
    }
}