using AutoMapper;
using BusinessLayer.DTOs.Order;
using DataAccessLayer.Entities;

namespace BusinessLayer.Mapper;

public class OrderMapper: Profile
{
    public OrderMapper()
    {
        CreateMap<Order, OrderDto>();
        CreateMap<OrderItem, OrderItemDto>();
    }
}