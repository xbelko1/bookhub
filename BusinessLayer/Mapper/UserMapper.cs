using AutoMapper;
using BusinessLayer.DTOs.User;
using DataAccessLayer.Entities;

namespace BusinessLayer.Mapper;

public class UserMapper: Profile
{
    public UserMapper()
    {
        CreateMap<User, OrderUserDto>();
    }
}