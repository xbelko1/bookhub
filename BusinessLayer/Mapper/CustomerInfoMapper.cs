using AutoMapper;
using BusinessLayer.DTOs.CustomerInfo;
using DataAccessLayer.Entities;

namespace BusinessLayer.Mapper;

public class CustomerInfoMapper: Profile
{
    public CustomerInfoMapper()
    {
        CreateMap<CustomerInfo, CustomerInfoDto>();
        CreateMap<CustomerInfoDto, CustomerInfo>();
    }
}