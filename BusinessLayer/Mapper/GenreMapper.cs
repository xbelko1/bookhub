using AutoMapper;
using BusinessLayer.DTOs.Genre;
using DataAccessLayer.Entities;

namespace BusinessLayer.Mapper;

public class GenreMapper: Profile
{
    public GenreMapper()
    {
        CreateMap<Genre, GenreDto>();
    }
}