using AutoMapper;
using BusinessLayer.DTOs.Book;
using DataAccessLayer.Entities;

namespace BusinessLayer.Mapper;

public class BookMapper : Profile
{
    public BookMapper()
    {
        CreateMap<Book, BookDto>();
        CreateMap<BookDto, Book>();
        CreateMap<BookCreateDto, Book>();
    }
}
