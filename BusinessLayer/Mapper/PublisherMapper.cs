using AutoMapper;
using BusinessLayer.DTOs.Publisher;
using DataAccessLayer.Entities;

namespace BusinessLayer.Mapper;

public class PublisherMapper: Profile
{
    public PublisherMapper()
    {
        CreateMap<Publisher, PublisherDto>();
    }
}