using BusinessLayer.DTOs.Author;
using BusinessLayer.DTOs.Genre;
using BusinessLayer.DTOs.Publisher;

namespace BusinessLayer.DTOs.Book;

public class BookDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int PublishYear { get; set; }
    public double Price { get; set; }
    public string Description { get; set; }
    public int PageNumber { get; set; }
    public GenreDto Genre { get; set; }
    public PublisherDto Publisher { get; set; }
    public AuthorDto Author { get; set; }
}
