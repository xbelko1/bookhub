namespace BusinessLayer.DTOs.Book;

public class BookCreateDto
{
    public string Name { get; set; }
    public int PublishYear { get; set; }
    public double Price { get; set; }
    public string Description { get; set; }
    public int PageNumber { get; set; }

    public int PublisherId { get; set; }
    public int GenreId { get; set; }
    public int AuthorId { get; set; }
}