namespace BusinessLayer.DTOs.Book;

public class BookFilterCriteria
{
    public string? Name { get; set; }
    public string? PublisherName { get; set; }
    public string? GenreName { get; set; }
    public string? AuthorLastName { get; set; }
    public decimal? MaxPrice { get; set; }
    public string? DescriptionContains { get; set; }
}
