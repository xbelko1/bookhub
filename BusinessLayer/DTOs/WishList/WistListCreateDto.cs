using System.ComponentModel.DataAnnotations;

namespace BusinessLayer.DTOs.WishList;

public class WistListCreateDto
{
    [Required]
    public string Name { get; set; }
}