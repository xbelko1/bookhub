using BusinessLayer.DTOs.WishListItem;

namespace BusinessLayer.DTOs.WishList;

public class WishListDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int UserId { get; set; }

    public IEnumerable<WishListItemDto> Items { get; set; } = new List<WishListItemDto>();
}