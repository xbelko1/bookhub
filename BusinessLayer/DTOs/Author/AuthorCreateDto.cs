using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BusinessLayer.DTOs.Author;

public class AuthorCreateDto
{
    [Required]
    [DefaultValue("Marek")]
    public string FirstName { get; set; }
    [Required]
    [DefaultValue("Macho")]
    public string LastName { get; set; }
    public int Age { get; set; }
}
