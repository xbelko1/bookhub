using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BusinessLayer.DTOs.Genre;

public class GenreCreateDto
{
    [Required]
    [DefaultValue("Sci-fi")]
    public string Name { get; set; }
}