namespace BusinessLayer.DTOs.CustomerInfo;

public class CustomerInfoDto
{
    public int PostCode { get; set; }
    public string Street { get; set; }
    public string City { get; set; }
    public int HouseNumber { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
}