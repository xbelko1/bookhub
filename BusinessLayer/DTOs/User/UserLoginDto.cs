using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace BusinessLayer.DTOs.User;

public class LoginUserDto
{
    [Required]
    [NotNull]
    [DefaultValue("Ferko2")]
    public string? UserName { get; set; }
    
    
    [Required]
    [NotNull]
    [DefaultValue("Password123")]
    [DataType(DataType.Password)]
    public string? Password { get; set; }
}