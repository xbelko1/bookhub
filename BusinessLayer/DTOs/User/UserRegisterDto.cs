using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BusinessLayer.DTOs.CustomerInfo;

namespace BusinessLayer.DTOs.User;
public class RegisterUserDto
{
    [Required]
    [DefaultValue("Ferko2")]
    public string UserName { get; set; }
    
    [Required]
    [DataType(DataType.Password)]
    [DefaultValue("Password123")]
    public string Password { get; set; }

    public CustomerInfoDto CustomerInfoDto { get; set; }
}