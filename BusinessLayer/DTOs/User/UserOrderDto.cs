using BusinessLayer.DTOs.CustomerInfo;

namespace BusinessLayer.DTOs.User;

public class OrderUserDto
{
    public int Id { get; set; }
    public string Username { get; set; }
    public CustomerInfoDto CustomerInfo { get; set; }
}