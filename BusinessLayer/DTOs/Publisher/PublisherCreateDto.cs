using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BusinessLayer.DTOs.Publisher;

public class PublisherCreateDto
{
    [Required]
    [DefaultValue("Albatros")]
    public string Name { get; set; }
}