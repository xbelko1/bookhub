namespace BusinessLayer.DTOs.Review;

public class ReviewDto
{
    public int Id { get; set; }
    public string Text { get; set; }
    public double Rating { get; set; }
    public int BookId { get; set; }
}
