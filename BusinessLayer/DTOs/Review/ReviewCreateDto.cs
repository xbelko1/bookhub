using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BusinessLayer.DTOs.Review;

public class ReviewCreateDto
{
    [Required]
    [DefaultValue("Not Great, Not Terrible")]
    public string Text { get; set; }
    [Required]
    [DefaultValue(7.7)]
    public double Rating { get; set; }
    public int BookId { get; set; }
}
