using BusinessLayer.DTOs.Book;

namespace BusinessLayer.DTOs.WishListItem;

public class WishListItemDto
{
    public int Id { get; set; }
    public BookDto Book { get; set; }
}