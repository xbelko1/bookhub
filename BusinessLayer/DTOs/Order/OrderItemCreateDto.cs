namespace BusinessLayer.DTOs.Order;

public class OrderItemCreateDto
{
    public int BookId { get; set; }
    public int Amount { get; set; }
}