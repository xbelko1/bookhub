using BusinessLayer.DTOs.User;

namespace BusinessLayer.DTOs.Order;

public class OrderDto
{
    public virtual IEnumerable<OrderItemDto> OrderItems { get; set; }
    public DateTime Date { get; set; }
    public OrderUserDto User { get; set; }
}