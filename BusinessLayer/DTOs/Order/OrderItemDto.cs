using BusinessLayer.DTOs.Book;

namespace BusinessLayer.DTOs.Order;

public class OrderItemDto
{
    public int Amount { get; set; }
    public double PricePerPiece { get; set; }
    public BookDto Book { get; set; }
}