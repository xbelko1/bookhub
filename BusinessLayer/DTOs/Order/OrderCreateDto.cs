namespace BusinessLayer.DTOs.Order;

public class OrderCreateDto
{
    public List<OrderItemCreateDto> OrderItems { get; set; }
}