using AutoMapper;
using BusinessLayer.DTOs.Publisher;
using BusinessLayer.Enums;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;

namespace BusinessLayer.Services;

public class PublisherService
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;
    public PublisherService(IPublisherRepository publisherRepository, IMapper mapper)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
    }
    
    public async Task<ServiceResult<PublisherDto>> AddPublisher(PublisherCreateDto publisherCreateDto)
    {
        Publisher publisher = new Publisher() { Name = publisherCreateDto.Name };
        var addedPublisher = await _publisherRepository.Add(publisher);

        return ServiceResult<PublisherDto>.Success(_mapper.Map<PublisherDto>(addedPublisher));
    }

    public async Task<ServiceResult<PublisherDto>> GetPublisherById(int id)
    {
        var publisher = await _publisherRepository.GetById(id);
        
        if (publisher != null)
        {
            return ServiceResult<PublisherDto>.Success(_mapper.Map<PublisherDto>(publisher));
        }

        return ServiceResult<PublisherDto>.Failure("Publisher not found", ResultCode.NotFound);
    }

    public async Task<ServiceResult<IEnumerable<PublisherDto>>> GetAllPublishers()
    {
        var publishers = await _publisherRepository.GetAll();
        return ServiceResult<IEnumerable<PublisherDto>>.Success(_mapper.Map<IEnumerable<PublisherDto>>(publishers));
    }

    public async Task<ServiceResult<PublisherDto>> DeletePublisher(int id)
    {
        var publisherToDelete = await _publisherRepository.GetById(id);
        
        if (publisherToDelete != null)
        {
            await _publisherRepository.Delete(publisherToDelete);
            return ServiceResult<PublisherDto>.Success(_mapper.Map<PublisherDto>(publisherToDelete));
        }
        
        return ServiceResult<PublisherDto>.Failure("Publisher not found", ResultCode.NotFound);
    }

    public async Task<ServiceResult<PublisherDto>> UpdatePublisher(int id, PublisherCreateDto publisherCreateDto)
    {
        var publisherToEdit = await _publisherRepository.GetById(id);
        
        if (publisherToEdit != null)
        {
            publisherToEdit.Name = publisherCreateDto.Name;
            
            await _publisherRepository.Update(publisherToEdit);
            return ServiceResult<PublisherDto>.Success(_mapper.Map<PublisherDto>(publisherToEdit));
        }
        
        return ServiceResult<PublisherDto>.Failure("Publisher not found", ResultCode.NotFound);
    }
}