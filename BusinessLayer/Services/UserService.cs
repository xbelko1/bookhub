using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BusinessLayer.Services;

public class UserService
{
    private readonly UserManager<DataAccessLayer.Entities.User> _userManager;

    public UserService(UserManager<DataAccessLayer.Entities.User> userManager)
    {
        _userManager = userManager;
    }

    public async Task<DataAccessLayer.Entities.User?> GetCurrentUser(int userId)
    {
        var user = await _userManager.Users
            .SingleOrDefaultAsync(u => u.Id == userId);
        
        return user;
    }
}