using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using BusinessLayer.DTOs.User;
using BusinessLayer.Enums;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace BusinessLayer.Services;

public class AuthService
{
    private readonly IConfiguration _config;
    private readonly UserManager<User> _userManager;
    private readonly SignInManager<User> _signInManager;
    private readonly IMapper _mapper;
    public AuthService(
        IConfiguration config,
        UserManager<User> userManager,
        SignInManager<User> signInManager, IMapper mapper
        )
    {
        _config = config;
        _userManager = userManager;
        _signInManager = signInManager;
        _mapper = mapper;
    }

    public async Task<ServiceResult<string>> LoginAsync(LoginUserDto userDto)
    {
        var user = await _userManager.FindByNameAsync(userDto.UserName);

        if (user == null)
        {
            return ServiceResult<string>.Failure($"Username: '{userDto.UserName}' not exist", ResultCode.NotFound);
        }

        var result = await _signInManager.PasswordSignInAsync(userDto.UserName, userDto.Password, false, false);

        if (!result.Succeeded)
        {
            return ServiceResult<string>.Failure("Invalid password", ResultCode.BadRequest);
        }

        return ServiceResult<string>.Success(CreateToken(userDto.UserName, user.Id));
    }

    public async Task<ServiceResult<string>> RegisterAsync(RegisterUserDto userDto)
    {
        var customerInfo = _mapper.Map<CustomerInfo>(userDto.CustomerInfoDto);
        
        var user = new User
        {
            UserName = userDto.UserName,
            CustomerInfo = customerInfo,
        };

        var result = await _userManager.CreateAsync(user, userDto.Password);

        if (!result.Succeeded)
        {
            return ServiceResult<string>.Failure(result.Errors.FirstOrDefault()?.Description, ResultCode.BadRequest);
        }

        return ServiceResult<string>.Success(CreateToken(userDto.UserName, user.Id));
    }
    
    public async Task<User> GetUserAsync(ClaimsPrincipal user)
    {
        return await _userManager.GetUserAsync(user);
    }

    public async Task SignOutAsync()
    {
        await _signInManager.SignOutAsync();
    }


    public string CreateToken(string username, int id)
    {
        var claims = new List<Claim>
        {
            new Claim(ClaimTypes.Name, username),
            new Claim(ClaimTypes.NameIdentifier, id.ToString())
        };
        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]!));
        var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
        
        var token = new JwtSecurityToken(
            claims: claims,
            expires: DateTime.Now.AddDays(5),
            signingCredentials: credentials);

        return new JwtSecurityTokenHandler().WriteToken(token);
    }
}