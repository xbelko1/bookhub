using System.Linq.Expressions;
using AutoMapper;
using BusinessLayer.DTOs.Book;
using BusinessLayer.Enums;
using BusinessLayer.Extensions;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;

namespace BusinessLayer.Services;

public class BookService
{
    private readonly IBookRepository _bookRepository;
    private readonly IGenreRepository _genreRepository;
    private readonly IPublisherRepository _publisherRepository;
    private readonly IAuthorRepository _authorRepository;
    private readonly IMapper _mapper;

    public BookService(IBookRepository bookRepository, IGenreRepository genreRepository, 
        IPublisherRepository publisherRepository, IAuthorRepository authorRepository, IMapper mapper)
    {
        _bookRepository = bookRepository;
        _genreRepository = genreRepository;
        _publisherRepository = publisherRepository;
        _authorRepository = authorRepository;
        _mapper = mapper;
    }
    
    public async Task<ServiceResult<BookDto>> AddBook(BookCreateDto book)
    {
        var genreExists = await _genreRepository.Exists(book.GenreId);
        var publisherExists = await _publisherRepository.Exists(book.PublisherId);
        var authorExists = await _authorRepository.Exists(book.AuthorId);
        
        if (!genreExists || !publisherExists || !authorExists)
            return ServiceResult<BookDto>.Failure("Publisher, Genre or Author not found", ResultCode.NotFound);
        var addedBook = await _bookRepository.Add(_mapper.Map<Book>(book));
        await _genreRepository.GetById(addedBook.GenreId);
        await _publisherRepository.GetById(addedBook.PublisherId);
        await _authorRepository.GetById(addedBook.AuthorId);
        return ServiceResult<BookDto>.Success(_mapper.Map<BookDto>(addedBook));

    }
    
    public async Task<ServiceResult<BookDto>> UpdateBook(int id, BookCreateDto updatedBook)
    {
        var bookToEdit = await _bookRepository.GetById(id);

        if (bookToEdit != null)
        {
            var genreExists = await _genreRepository.Exists(updatedBook.GenreId);
            var publisherExists = await _publisherRepository.Exists(updatedBook.PublisherId);
            var authorExists = await _authorRepository.Exists(updatedBook.AuthorId);

            if (genreExists && publisherExists && authorExists)
            {
                await _bookRepository.Update(_mapper.Map(updatedBook, bookToEdit));

                return ServiceResult<BookDto>.Success(_mapper.Map<BookDto>(bookToEdit));
            }

            return ServiceResult<BookDto>.Failure("Publisher, Author or Genre not found", ResultCode.NotFound);
        }

        return ServiceResult<BookDto>.Failure("Book not found", ResultCode.NotFound);
    }

    public async Task<ServiceResult<BookDto>> GetBookById(int id)
    {
        var book = await _bookRepository.GetById(id);
        
        return book != null
            ? ServiceResult<BookDto>.Success(_mapper.Map<BookDto>(book)) 
            : ServiceResult<BookDto>.Failure("Book not found", ResultCode.NotFound);
    }

    public async Task<ServiceResult<IEnumerable<BookDto>>> GetAllBooks()
    {
        var books = await _bookRepository.GetAll();
        return ServiceResult<IEnumerable<BookDto>>.Success(_mapper.Map<IEnumerable<BookDto>>(books));
    }
    
    public async Task<ServiceResult<BookDto>> DeleteBook(int id)
    {
        var bookToDelete = await _bookRepository.GetById(id);

        if (bookToDelete == null) return ServiceResult<BookDto>.Failure("Book not found", ResultCode.NotFound);
        await _bookRepository.Delete(bookToDelete);
        return ServiceResult<BookDto>.Success(_mapper.Map<BookDto>(bookToDelete));

    }
    
    public async Task<ServiceResult<IEnumerable<BookDto>>> FilterBooks(BookFilterCriteria filterCriteria)
    {
        Predicate<Book> filter = book =>
            book.MatchesName(filterCriteria.Name) &&
            book.MatchesPublisherName(filterCriteria.PublisherName) &&
            book.MatchesGenreName(filterCriteria.GenreName) &&
            book.MatchesAuthorLastName(filterCriteria.AuthorLastName) &&
            book.MatchesMaxPrice(filterCriteria.MaxPrice) &&
            book.MatchesDescriptionContains(filterCriteria.DescriptionContains);
        
        var filteredBooks = await _bookRepository.Filter(filter);

        return filteredBooks.Any()
            ? ServiceResult<IEnumerable<BookDto>>.Success(_mapper.Map<IEnumerable<BookDto>>(filteredBooks))
            : ServiceResult<IEnumerable<BookDto>>.Failure("No books found with the specified criteria.", ResultCode.NotFound);
    }
}
