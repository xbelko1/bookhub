using BusinessLayer.Enums;

namespace BusinessLayer.Services;

public class ServiceResult<T>
{
    public T? Data { get; set; }
    public string? ErrorMessage { get; set; }
    public ResultCode ResultCode { get; set; }
    
    public static ServiceResult<T> Success(T data, ResultCode resultCode = ResultCode.Ok)
    {
        return new ServiceResult<T>
        {
            Data = data,
            ErrorMessage = null,
            ResultCode = resultCode
        };
    }

    public static ServiceResult<T> Failure(string? errorMessage, ResultCode resultCode)
    {
        return new ServiceResult<T>
        {
            Data = default,
            ErrorMessage = errorMessage,
            ResultCode = resultCode
        };
    }

}