using AutoMapper;
using BusinessLayer.DTOs.Order;
using BusinessLayer.Enums;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;

namespace BusinessLayer.Services;

public class OrderService
{
    private readonly IOrderRepository _orderRepository;
    private readonly IBookRepository _bookRepository;
    private readonly IMapper _mapper;

    public OrderService(IOrderRepository orderRepository, IBookRepository bookRepository, IMapper mapper)
    {
        _orderRepository = orderRepository;
        _bookRepository = bookRepository;
        _mapper = mapper;
    }


    public async Task<ServiceResult<OrderDto>> AddOrder(OrderCreateDto orderCreateDto, int userId)
    {
        var orderItems = new List<OrderItem>();
        
        foreach (var item in orderCreateDto.OrderItems)
        {
            var book = await _bookRepository.GetById(item.BookId);
            if (book != null)
            {
                orderItems.Add(new OrderItem()
                {
                    BookId = item.BookId,
                    Amount = item.Amount,
                    PricePerPiece = book.Price
                });
            }
            else
            {
                return ServiceResult<OrderDto>.Failure($"BookId {item.BookId} not found", ResultCode.NotFound);
            }
        }

        var order = new Order()
        {
            OrderItems = orderItems,
            UserId = userId
        };

        var addedOrder = await _orderRepository.Add(order);

        return ServiceResult<OrderDto>.Success(_mapper.Map<OrderDto>(addedOrder));
    }
    
    public async Task<ServiceResult<OrderDto>> GetOrderById(int id, int userId)
    {
        var order = await _orderRepository.GetById(id);
        
        if (order != null && userId == order.UserId)
        {
            return ServiceResult<OrderDto>.Success(_mapper.Map<OrderDto>(order));
        }
        else if (order != null && userId != order.UserId)
        {
            return ServiceResult<OrderDto>.Failure("User id not equal", ResultCode.Unauthorized);
        }

        return ServiceResult<OrderDto>.Failure("Order not found", ResultCode.NotFound);
    }

    public async Task<ServiceResult<IEnumerable<OrderDto>>> GetAllUserOrders(int userId)
    {
        var orders = await _orderRepository.Filter(x => x.UserId == userId);

        return ServiceResult<IEnumerable<OrderDto>>.Success(_mapper.Map<IEnumerable<OrderDto>>(orders));
    }
    
    public async Task<ServiceResult<IEnumerable<OrderDto>>> GetAllOrders()
    {
        var orders = await _orderRepository.GetAll();

        return ServiceResult<IEnumerable<OrderDto>>.Success(_mapper.Map<IEnumerable<OrderDto>>(orders));
    }
 
}