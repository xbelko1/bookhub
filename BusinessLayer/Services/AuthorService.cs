using AutoMapper;
using BusinessLayer.DTOs.Author;
using BusinessLayer.Enums;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;

namespace BusinessLayer.Services;

public class AuthorService
{
    private readonly IAuthorRepository _authorRepository;
    private readonly IMapper _mapper;
    
    public AuthorService(IAuthorRepository authorRepository, IMapper mapper)
    {
        _authorRepository = authorRepository;
        _mapper = mapper;
    }

    public async Task<ServiceResult<AuthorDto>> AddAuthor(AuthorCreateDto authorCreateDto)
    {
        var addedAuthor = await _authorRepository.Add(_mapper.Map<Author>(authorCreateDto));
        return ServiceResult<AuthorDto>.Success(_mapper.Map<AuthorDto>(addedAuthor));
    }

    public async Task<ServiceResult<AuthorDto>> DeleteAuthor(int id)
    {
        var author = await _authorRepository.GetById(id);
        if (author == null) return ServiceResult<AuthorDto>.Failure("Author not found", ResultCode.NotFound);
        await _authorRepository.Delete(author);
        return ServiceResult<AuthorDto>.Success(_mapper.Map<AuthorDto>(author));
    }

    public async Task<ServiceResult<AuthorDto>> GetAuthorById(int id)
    {
        var author = await _authorRepository.GetById(id);
        return author != null 
            ? ServiceResult<AuthorDto>.Success(_mapper.Map<AuthorDto>(author))
            : ServiceResult<AuthorDto>.Failure("Author not found", ResultCode.NotFound);
    }

    public async Task<ServiceResult<IEnumerable<AuthorDto>>> GetAllAuthors()
    {
        var authors = await _authorRepository.GetAll();
        return ServiceResult<IEnumerable<AuthorDto>>.Success(_mapper.Map<IEnumerable<AuthorDto>>(authors));
    }

    public async Task<ServiceResult<AuthorDto>> UpdateAuthor(int id, AuthorCreateDto authorCreateDto)
    {
        var author = await _authorRepository.GetById(id);

        if (author == null) return ServiceResult<AuthorDto>.Failure("Author not found", ResultCode.NotFound);
        await _authorRepository.Update(_mapper.Map(authorCreateDto, author));
        return ServiceResult<AuthorDto>.Success(_mapper.Map<AuthorDto>(author));
    }
}
