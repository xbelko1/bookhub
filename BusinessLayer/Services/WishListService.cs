using AutoMapper;
using BusinessLayer.DTOs.WishList;
using BusinessLayer.Enums;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;

namespace BusinessLayer.Services;

public class WishListService
{
    private readonly IWishListRepository _wishListRepository;
    private readonly IMapper _mapper;
    private readonly IBookRepository _bookRepository;

    public WishListService(IWishListRepository wishListRepository, IMapper mapper, IBookRepository bookRepository)
    {
        _wishListRepository = wishListRepository;
        _mapper = mapper;
        _bookRepository = bookRepository;
    }

    public async Task<ServiceResult<WishListDto>> AddWishList(int userId, WistListCreateDto wishList)
    {
        var addedWishList = await _wishListRepository.Add(new WishList()
        {
            Name = wishList.Name,
            UserId = userId
        });

        return ServiceResult<WishListDto>.Success(_mapper.Map<WishListDto>(addedWishList));
    }
    
    public async Task<ServiceResult<WishListDto>> AddBookToWishList(int wishListId, int bookId)
    {
        var wishList = await _wishListRepository.GetById(wishListId);
        if (wishList == null)
        {
            return ServiceResult<WishListDto>.Failure("WishList not found", ResultCode.NotFound);
        }

        var book = await _bookRepository.GetById(bookId);
        if (book == null)
        {
            return ServiceResult<WishListDto>.Failure("Book not found", ResultCode.NotFound);
        }

        wishList.Items.Add(new WishListItem()
        {
            Book = book,
            WishlistId = wishList.Id
        });

        var updatedWishList = await _wishListRepository.Update(wishList);

        return ServiceResult<WishListDto>.Success(_mapper.Map<WishListDto>(updatedWishList));
    }

    public async Task<ServiceResult<WishListDto>> GetWishListById(int wishListId)
    {
        var wishList = await _wishListRepository.GetById(wishListId);
        if (wishList != null)
        {
            return ServiceResult<WishListDto>.Success(_mapper.Map<WishListDto>(wishList));
        }

        return ServiceResult<WishListDto>.Failure("WishList not found.", ResultCode.NotFound);
    }
    
    public async Task<ServiceResult<IEnumerable<WishListDto>>> GetAllWishLists()
    {
        var wishLists = await _wishListRepository.GetAll();
        return ServiceResult<IEnumerable<WishListDto>>.Success(_mapper.Map<List<WishListDto>>(wishLists));
    }
    
    public async Task<ServiceResult<WishListDto>> DeleteWishList(int id)
    {
        var wishListToDelete = await _wishListRepository.GetById(id);
        if (wishListToDelete != null)
        {
            await _wishListRepository.Delete(wishListToDelete);
            return ServiceResult<WishListDto>.Success(_mapper.Map<WishListDto>(wishListToDelete));
        }
        
        return ServiceResult<WishListDto>.Failure("Book not found", ResultCode.NotFound);
    }

    public async Task<ServiceResult<WishListDto>> DeleteBookFromWishList(int wishListId, int bookId)
    {
        var wishList = await _wishListRepository.GetById(wishListId);
        if (wishList == null)
        {
            return ServiceResult<WishListDto>.Failure("WishList not found", ResultCode.NotFound);
        }
        
        var wishListItem = wishList.Items.FirstOrDefault(item => item.BookId == bookId);
        if (wishListItem == null)
        {
            return ServiceResult<WishListDto>.Failure("Book not found in the wish list", ResultCode.NotFound);
        }

        wishList.Items.Remove(wishListItem);
        var updatedWishList = await _wishListRepository.Update(wishList);

        return ServiceResult<WishListDto>.Success(_mapper.Map<WishListDto>(updatedWishList));
    }
}