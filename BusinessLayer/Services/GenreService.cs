using AutoMapper;
using BusinessLayer.DTOs.Genre;
using BusinessLayer.Enums;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;

namespace BusinessLayer.Services;
public class GenreService
{
    private readonly IGenreRepository _genreRepository;
    private readonly IMapper _mapper;

    public GenreService(IGenreRepository genreRepository, IMapper mapper)
    {
        _genreRepository = genreRepository;
        _mapper = mapper;
    }
    
    public async Task<ServiceResult<GenreDto>> AddGenre(GenreCreateDto genreCreateDto)
    {
        Genre genre = new Genre() { Name = genreCreateDto.Name };
        var addedGenre = await _genreRepository.Add(genre);

        return ServiceResult<GenreDto>.Success(_mapper.Map<GenreDto>(addedGenre));
    }

    public async Task<ServiceResult<GenreDto>> GetGenreById(int id)
    {
        var genre = await _genreRepository.GetById(id);
        
        if (genre != null)
        {
            return ServiceResult<GenreDto>.Success(_mapper.Map<GenreDto>(genre));
        }

        return ServiceResult<GenreDto>.Failure("Genre not found", ResultCode.NotFound);
    }

    public async Task<ServiceResult<IEnumerable<GenreDto>>> GetAllGenres()
    {
        var genres = await _genreRepository.GetAll();
        return ServiceResult<IEnumerable<GenreDto>>.Success(_mapper.Map<IEnumerable<GenreDto>>(genres));
    }

    public async Task<ServiceResult<GenreDto>> DeleteGenre(int id)
    {
        var genreToDelete = await _genreRepository.GetById(id);
        
        if (genreToDelete != null)
        {
            await _genreRepository.Delete(genreToDelete);
            return ServiceResult<GenreDto>.Success(_mapper.Map<GenreDto>(genreToDelete));
        }
        
        return ServiceResult<GenreDto>.Failure("Genre not found", ResultCode.NotFound);
    }

    public async Task<ServiceResult<GenreDto>> UpdateGenre(int id, GenreCreateDto genreCreateDto)
    {
        var genreToEdit = await _genreRepository.GetById(id);
        
        if (genreToEdit != null)
        {
            genreToEdit.Name = genreCreateDto.Name;
            
            await _genreRepository.Update(genreToEdit);
            return ServiceResult<GenreDto>.Success(_mapper.Map<GenreDto>(genreToEdit));
        }
        
        return ServiceResult<GenreDto>.Failure("Genre not found", ResultCode.NotFound);
    }
}