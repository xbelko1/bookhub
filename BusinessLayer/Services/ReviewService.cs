using AutoMapper;
using BusinessLayer.DTOs.Review;
using BusinessLayer.Enums;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;

namespace BusinessLayer.Services;

public class ReviewService
{
    private readonly IReviewRepository _reviewRepository;
    private readonly IMapper _mapper;
    private readonly int _maxRating = 10;
    private readonly int _minRating = 0;
    
    public ReviewService(IReviewRepository reviewRepository, IMapper mapper)
    {
        _reviewRepository = reviewRepository;
        _mapper = mapper;
    }
    
    public async Task<ServiceResult<ReviewDto>> AddReview(int userId, ReviewCreateDto review)
    {
        if(IsRatingIncorrect(review.Rating)) 
            return ServiceResult<ReviewDto>.Failure("Rating is incorrect", ResultCode.BadRequest); 
        
        var addedReview = await _reviewRepository.Add(new Review
        {
            Text = review.Text,
            Rating = review.Rating,
            UserId = userId,
            BookId = review.BookId
        });

        return ServiceResult<ReviewDto>.Success(_mapper.Map<ReviewDto>(addedReview));
    }
    
    public async Task<ServiceResult<ReviewDto>> DeleteReview(int id)
    {
        var review = await _reviewRepository.GetById(id);
        if (review == null) return ServiceResult<ReviewDto>.Failure("Review not found", ResultCode.NotFound);
        await _reviewRepository.Delete(review);
        return ServiceResult<ReviewDto>.Success(_mapper.Map<ReviewDto>(review));
    }

    public async Task<ServiceResult<ReviewDto>> GetReviewById(int id)
    {
        var review = await _reviewRepository.GetById(id);
        return review != null
            ? ServiceResult<ReviewDto>.Success(_mapper.Map<ReviewDto>(review))
            : ServiceResult<ReviewDto>.Failure("Review not found", ResultCode.NotFound);
    }

    public async Task<ServiceResult<IEnumerable<ReviewDto>>> GetAllReviews()
    {
        var reviews = await _reviewRepository.GetAll();
        return ServiceResult<IEnumerable<ReviewDto>>.Success(_mapper.Map<IEnumerable<ReviewDto>>(reviews));
    }

    public async Task<ServiceResult<ReviewDto>> UpdateReview(int id, ReviewCreateDto reviewCreateDto)
    {
        if(IsRatingIncorrect(reviewCreateDto.Rating)) 
            return ServiceResult<ReviewDto>.Failure("Rating is incorrect", ResultCode.BadRequest);
        
        var review = await _reviewRepository.GetById(id);

        if (review == null) return ServiceResult<ReviewDto>.Failure("Review not found", ResultCode.NotFound);
        await _reviewRepository.Update(_mapper.Map(reviewCreateDto, review));
        return ServiceResult<ReviewDto>.Success(_mapper.Map<ReviewDto>(review));
    }

    private bool IsRatingIncorrect(double rating)
    {
        return rating < _minRating || rating > _maxRating;
    }
}
