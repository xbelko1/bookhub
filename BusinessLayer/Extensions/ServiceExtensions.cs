using BusinessLayer.Services;
using DataAccessLayer.Entities;
using Microsoft.Extensions.DependencyInjection;

namespace BusinessLayer.Extensions;

public static class ServiceExtensions
{
    public static IServiceCollection RegisterServices(this IServiceCollection services)
    {
        services.AddTransient<AuthService>();
        services.AddTransient<UserService>();
        services.AddTransient<BookService>();
        services.AddTransient<WishListService>();
        services.AddTransient<GenreService>();
        services.AddTransient<PublisherService>();
        services.AddTransient<AuthorService>();
        services.AddTransient<OrderService>();
        services.AddTransient<ReviewService>();
        
        return services;
    }
}

public static class BookExtensions
{
    public static bool MatchesName(this Book book, string? name)
    {
        return name == null || book.Name.ToLower().Contains(name.ToLower());
    }

    public static bool MatchesPublisherName(this Book book, string? publisherName)
    {
        return publisherName == null || book.Publisher.Name.ToLower().Contains(publisherName.ToLower());
    }

    public static bool MatchesGenreName(this Book book, string? genreName)
    {
        return genreName == null || book.Genre.Name.ToLower().Contains(genreName.ToLower());
    }

    public static bool MatchesAuthorLastName(this Book book, string? authorLastName)
    {
        return authorLastName == null || book.Author.LastName.ToLower().Contains(authorLastName.ToLower());
    }

    public static bool MatchesMaxPrice(this Book book, decimal? maxPrice)
    {
        return !maxPrice.HasValue || book.Price <= maxPrice;
    }

    public static bool MatchesDescriptionContains(this Book book, string? descriptionContains)
    {
        return descriptionContains == null || book.Description.ToLower().Contains(descriptionContains.ToLower());
    }
}
