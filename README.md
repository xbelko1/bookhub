# BookHub
Project for subject PV179 @ MUNI FI

## Authors:
- Erik Belko - [xbelko1](https://gitlab.fi.muni.cz/xbelko1)
- Martin Mlýnek - [xmlynek](https://gitlab.fi.muni.cz/xmlynek)
- Ján Homola - [xhomola](https://gitlab.fi.muni.cz/xhomola)

## Team Roles:
- Team Lead: Ján Homola
- Team Members: Erik Belko, Martin Mlýnek

## Table of Contents
- [About](#about)
- [Getting Started](#getting-started)
- [Technical Overview](#technical-overview)
- [Usage](#usage)

## About
Digital platform for the company called "BookHub", a company that sells books of various genres. The platform provides easy browsing and purchase of books, letting customers sort and filter by authors, publishers, and genres. After customers create accounts, they are able to review their purchase history, rate books, and make wishlists. Administrators have privileges to update book details, manage user accounts, and regulate book prices.

## Development document
[Shared Google document](https://docs.google.com/document/d/1Y02bRv8s99LC1ESPWG8q-JVPDhehz7eAnbNF1KK9jIo/edit?usp=sharing)

## Getting Started

### Run Database in Docker
- from docker-db/ run
  - ON: `sudo docker-compose up`
  - OFF: `sudo docker-compose down`

### Database setup
- from WebApi/ run:  
  - Start database
  - `dotnet ef migrations add InitMigration --project ../DataAccessLayer`
  - `dotnet ef database update`
  - (Optional): Stop database
- this will seed data into database

### Running the Application
- Start database
- Run application to test database logic using ***Swagger***
- Firstly please do ***Authorise***
- Now you can use all endpoints
- Stop database

### Cleanup
- After stopping the application, for database cleanup DROP tables and from WebApi/ run:
  - `dotnet ef migrations remove --project ../DataAccessLayer`

## Technical Overview
The controllers are the initial entry points for managing incoming API requests. Their primary role is to receive these requests and efficiently route them to the relevant service layers.

Within the service layers, the core business logic of the application is executed. This logic encompasses tasks such as processing user requests, enforcing business rules, and orchestrating data operations. Services encapsulate and abstract the critical application logic while providing a clear separation of concerns.

Data management in this architecture is primarily handled through an SQLite database. To facilitate this interaction, repositories act as intermediaries. Repositories bridge the gap between the services and the database, enabling operations like data retrieval, insertion, updates, and deletions.

### ER Diagram
![ER diagram](/docs/diagrams/BookHub-erDiagram.png)

### Use Case Diagram
![UC diagram](/docs/diagrams/BookHub-useCase.png)

### Projects
- BusinessLayer
- BusinessLayer.Tests
- DAL
- Infrastructure
- MVC
- WebApi

### MVC
- Auth - register, login and logout 
  - after successful login, users can retrieve an authentication token for WebAPI.

### Testing
- Various tests of implemented services are located in BusinessLayer.Tests project
- tests can be run from root directory using:
  - `dotnet test`

### API Endpoints
- Auth
- Author
- Book
- Genre
- Order
- Publisher
- Review
- User
- WishList

### Technology Stack
- C#
- Docker
- Postgress
- ASP.Net
- Swagger
- xUnit
- NSubstitute
